#!/bin/bash

INFILE='src/inputs/day6.txt'

MIN_X=`sort -n -k1 $INFILE | head -1 | awk -F', ' '{print $1}'`
MAX_X=`sort -n -k1 $INFILE | tail -1 | awk -F', ' '{print $1}'`
MIN_Y=`sort -n -k2 $INFILE | head -1 | awk -F', ' '{print $2}'`
MAX_Y=`sort -n -k2 $INFILE | tail -1 | awk -F', ' '{print $2}'`
POINTS_X=(`awk -F', ' '{print $1}' $INFILE`)
POINTS_Y=(`awk -F', ' '{print $2}' $INFILE`)

NUM_POINTS=${#POINTS_X[@]}

edges=()
areas=()
safe_count=0

for ((x=$MIN_X;x<=$MAX_X;x++)); do
  for ((y=$MIN_Y;y<=$MAX_Y;y++)); do
    MIN_MANHATTAN=$((MAX_X+MAX_Y+99))
    MIN_SLOT=-1
    SUM_MANHATTAN=0
    for ((i=0;i<$NUM_POINTS;i++)); do
      px=${POINTS_X[$i]}
      py=${POINTS_Y[$i]}
      dist_x=$((px-x))
      dist_x=${dist_x#-}
      dist_y=$((py-y))
      dist_y=${dist_y#-}
      manhattan=$((dist_x+dist_y))
      if [ $manhattan -lt $MIN_MANHATTAN ]; then
        MIN_MANHATTAN=$manhattan
        MIN_SLOT=$i
      elif [ $manhattan == $MIN_MANHATTAN ]; then
        MIN_SLOT=-1
      fi
      SUM_MANHATTAN=$((SUM_MANHATTAN+manhattan))
    done
    if [ $SUM_MANHATTAN -lt 10000 ]; then
      safe_count=$((safe_count+=1))
    fi

    if [ $MIN_SLOT != -1 ]; then
      curr=${areas[$MIN_SLOT]}
      areas[$MIN_SLOT]=$((${curr:=0} + 1))
      if [ $x == $MIN_X -o $x == $MAX_X -o $y == $MIN_Y -o $y == $MAX_Y ]; then
        # keep track of points that connect to edge (implies infinite area)
        edges[$MIN_SLOT]=1
      fi
    fi
  done
done

for point_id in "${!areas[@]}"; do
  if [ "${edges[$point_id]}" != "1" ]; then
    echo "$point_id ${areas[$point_id]}"
  fi
done | sort -n -k2 | tail -1 | awk '{print "Part 1 - Answer: " $NF}'

echo "Part 2 - Answer: ${safe_count}"