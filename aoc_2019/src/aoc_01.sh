#!/bin/bash

echo "Day 1"

awk '{val+=$1} END {print "Part 1 - Answer: " val}' src/inputs/day1.txt

while cat src/inputs/day1.txt; do :;done | awk 'NF {sum+=$1;if (seen[sum]=="y"){print "Part 2 - Answer: " sum; exit} seen[sum]="y"}'