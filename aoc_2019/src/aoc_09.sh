#!/bin/bash
DEBUG=0
PLAYERS=416
MARBLES=71975

MARBLES_WORD="$MARBLES"
WORD_WIDTH=${#MARBLES_WORD}
WORD_WIDTH=$((WORD_WIDTH+1))

BLANK_MARBLE=""
for ((i=0;i<$WORD_WIDTH;i++)); do
  BLANK_MARBLE="${BLANK_MARBLE} "
done

LAST_MARBLE_VAL="0"

make_marble_word(){
  LAST_MARBLE_VAL=$1
  mmw_len=${#LAST_MARBLE_VAL}
  mmw_pad=$((WORD_WIDTH-mmw_len))
  if ((mmw_pad > 0)); then
    LAST_MARBLE_VAL="${BLANK_MARBLE:0:$mmw_pad}$1"
  fi
}

make_marble_word "0"

CIRCLE=$LAST_MARBLE_VAL

SCORES=()
CURRENT_MARBLE=0


print_circle() {
  if ((DEBUG==1)); then
    echo $CIRCLE
  fi
}

remove_marble() {
  rm_marb_index=$1
  rm_offset=$((rm_marb_index * WORD_WIDTH))
  LAST_MARBLE_VAL="${CIRCLE:$rm_offset:$WORD_WIDTH}"
  CIRCLE="${CIRCLE:0:$rm_offset}${CIRCLE:$((rm_offset+WORD_WIDTH)):${#CIRCLE}}"
}

insert_marble() {
  im_marb_index=$1
  im_marble=$2

  im_offset=$((im_marb_index * WORD_WIDTH))
  make_marble_word "${im_marble}"

  CIRCLE="${CIRCLE:0:$im_offset}${LAST_MARBLE_VAL}${CIRCLE:$im_offset:${#CIRCLE}}"
}

marble_value() {
  mv_marb_index=$1
  mv_offset=$((mv_marb_index * WORD_WIDTH))
  LAST_MARBLE_VAL="${CIRCLE:$mv_offset:$WORD_WIDTH}"
}

for ((m=1;m<=$MARBLES;m++)) {
  curr_player=$(( ((m-1) % PLAYERS) + 1))
  circle_size="${#CIRCLE}"
  circle_size=$((circle_size / WORD_WIDTH ))

  if ((m % 23 == 0)); then
    # todo if (CURRENT_MARBLE-7) is negative, ...wraps to end... so circle_size-diff?
    target=$(( (CURRENT_MARBLE-7) % (circle_size-1) ))
    if ((target < 0)); then
      target=$((circle_size+target))
    fi

    CURRENT_MARBLE=$target
    marble_value "${target}"
    #echo "player ${curr_player} scored for ${m} removing marble[${target}] ($LAST_MARBLE_VAL)!"
    print_circle
    remove_marble $target
    print_circle
    #read foo
    target_val="$LAST_MARBLE_VAL"
    #echo "player ${curr_player} scored! target ${target}, target_val ${target_val}, circle size $circle_size"
    score_val=$((SCORES[$curr_player]+m+target_val))
    #echo "score $score_val"
    SCORES[$curr_player]=$score_val
    continue
  fi
  play_pos=$(( (CURRENT_MARBLE+2) % circle_size ))
  if (( play_pos == 0 )); then
    play_pos=$circle_size
  fi
  insert_marble "$play_pos" "$m"
  CURRENT_MARBLE=$play_pos
  #echo -e "\n${curr_player} played marble ${m} at position $play_pos"
  print_circle
}

for ((i=0;i<=$PLAYERS;i++)); do
  p_score="${SCORES[$i]}"
  if (( p_score > 0 )); then
    echo "Player $((i)) Scored: ${SCORES[$i]}"
  fi
done | sort -k4 -n