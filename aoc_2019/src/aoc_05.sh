#!/bin/bash

REACT_PAIRS=`for l in {A..Z}; do echo $l; done | awk '{print $0 tolower($0);print tolower($0) $0}' | tr '\n' '|'`
REACT_PAIRS=${REACT_PAIRS::${#REACT_PAIRS}-1}

collapse() {
  SEQUENCE=$1
  SEQ_LENGTH=${#SEQUENCE}
  DIFF=-1
  while [ $DIFF != 0 ]; do
    SEQUENCE=`echo $SEQUENCE | sed -E "s/${REACT_PAIRS}//g"`
    UPDATED_LEN=${#SEQUENCE}
    DIFF=$((SEQ_LENGTH-UPDATED_LEN))
    SEQ_LENGTH=$UPDATED_LEN
  done
  echo $SEQ_LENGTH
}

collapse_worker() {
  collapse "$1" >> /tmp/day5_part2_out.txt
}

FULL_SEQ=`cat src/inputs/day5.txt`

ANSWER=`collapse "${FULL_SEQ}"`
echo "Part 1 - Answer: ${ANSWER}"

>/tmp/day5_part2_out.txt

pids=""
# if mac used gnu sed, coud just use `/I` for case insensitive at end of the sed
for letter in `for l in {A..Z}; do echo $l; done | awk '{print $0 "|" tolower($0)}'`; do
  ADJUSTED_SEQ=`echo ${FULL_SEQ} | sed -E "s/$letter//g"`
  collapse_worker "${ADJUSTED_SEQ}" &
  pids+=" $!"
done

for pid in $pids; do
  wait $pid
done
sort -n /tmp/day5_part2_out.txt | head -1 | awk '{print "Part 2 - Answer: " $1}'