#!/bin/bash

awk '{print $8 " " $2}' src/inputs/day7.txt | sort | awk '
function lowest(arr){
  best=0
  for (i=0;i<length(val);i++) {
    if (arr[i] < arr[best]) {
      best=i
    }
  }
  return arr[best]
}
{
  deps[$1]=$2 " " deps[$1]
  deps[$2]=deps[$2]
}
END {
  while (length(deps)>0) {
    delete candidates
    for (node in deps) {
      if (deps[node] == "") {
        candidates[length(candidates)]=node
      }
    }
    best=lowest(candidates)
    print best
    delete deps[best]
    for (node in deps) {
      gsub(best, "", deps[node])
      gsub(/^[ \t]+/, "", deps[node])
    }
  }
}' | tr -d '\n' | awk '{print "Part 1 - Answer: " $0}'


awk '{print $8 " " $2}' src/inputs/day7.txt | sort | awk '
function pop_lowest(arr){
  best=0
  for (i=0;i<length(arr);i++) {
    if (arr[best] == "" || (arr[i] != "" && arr[i] < arr[best])) {
      best=i
    }
  }
  best_val=arr[best]
  delete arr[best]
  return best_val
}
function next_worker(workers, max) {
  for (i=0;i<max;i++) {
    if (workers[i] == "") {
      return i
    }
  }
  return 0
}
function task_time(task) {
  split("ABCDEFGHIJKLMNOPQRSTUVWXYZ", chars, "")
  for (i=0; i<=27; i++) {
    if (chars[i]==task) {
      return i
    }
  }
  return 0
}
function print_workers(worker_tasks) {
  result=""
  for (i=0;i<total_workers;i++) {
    if (i>0) {
      result = result "\t"
    }
    result = result or_dot(worker_tasks[i])
  }
  return result
}
function or_dot(str) {
  if (str == "") {
    return "."
  }
  return str
}
BEGIN {
  total_workers=5
  moar_work=60
}
{
  deps[$1]=$2 " " deps[$1]
  deps[$2]=deps[$2]
}
END {
  steps_unfinished=length(deps)
  curr_second=0
  ready_workers=total_workers
  for (dep in deps) {
    print dep "->" deps[dep]
  }

  while (steps_unfinished>0) {
    delete candidates
    for (node in deps) {
      if (deps[node] == "") {
        candidates[length(candidates)]=node
      }
    }
    # assign as much work as possible
    while (length(candidates)>0 && ready_workers>0) {
      delete candidates
      for (node in deps) {
        if (deps[node] == "") {
          candidates[length(candidates)]=node
        }
      }
      #print "rw:" ready_workers ",nc:" length(candidates)
      best=pop_lowest(candidates)
      delete deps[best]
      wid=next_worker(worker_tasks, total_workers)
      worker_tasks[wid]=best
      time_total=task_time(best)+moar_work
      worker_time[wid]=time_total
      ready_workers-=1
    }
    print curr_second "\t" print_workers(worker_tasks) "\t" done_list

    # the second finishes, we increment time, and decrement work
    curr_second+=1
    for (w=0; w<total_workers; w++) {
      task=worker_tasks[w]
      if (task == "") {
        continue
      }
      worker_time[w]-=1
      if (worker_time[w] <= 0) {
        delete worker_tasks[w]
        delete worker_time[w]
        ready_workers+=1
        done_list=done_list "" task
        steps_unfinished-=1

        for (node in deps) {
          gsub(task, "", deps[node])
          gsub(/^[ \t]+/, "", deps[node])
        }
      }
    }
  }
  print curr_second "\t" print_workers(worker_tasks) "\t" done_list
}'

