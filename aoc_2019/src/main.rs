use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::collections::HashSet;
use std::collections::HashMap;

fn main() {
    let (problem, fname) = parse_args();

    match problem.as_ref() {
        "1" => day1(&fname),
        "2" => day2(&fname),
        "3" => day3(&fname),
        "4" => day4(&fname),
        _ => panic!("unknown problem {}", problem)
    }
}

fn parse_args() -> (String, String) {
    let args: Vec<String> = env::args().collect();
    match args.len() {
        2 => (args.get(1).unwrap().to_string(), "src/inputs/day".to_string()+args.get(1).unwrap()+".txt"),
        3 => (args.get(1).unwrap().to_string(), args.get(2).unwrap().to_string()),
        _ => panic!("usage: advent_2018 <day> <input>"),
    }
}

fn day1(fname: &str) {
    day1_part1(fname);
    day1_part2(fname);
}

fn day1_part1(fname: &str) {
    let reader = BufReader::new(File::open(fname).unwrap());
    let mut sum = 0;
    for line in reader.lines() {
        if let Ok(val) = line.unwrap().parse::<i64>() {
            sum += val;
        }
    }
    println!("{}", sum);
}

fn day1_part2(fname: &str) {
    let mut seen = HashSet::new();
    let mut freq = 0;

    seen.insert(freq);
    loop {
        let reader = BufReader::new(File::open(fname).unwrap());
        for line in reader.lines() {
            if let Ok(val) = line.unwrap().parse::<i64>() {
                freq += val;
                if !seen.insert(freq) {
                    println!("{} seen again!", freq);
                    return;
                }
            }
        }
    }
}

fn day2(fname: &str) {
    let mut vals1 = Vec::new();

    let reader = BufReader::new(File::open(fname).unwrap());
    for line in reader.lines() {
        vals1.push(line.unwrap());
    }
    let vals2 = vals1.clone();
    for val1 in vals1 {
        let val1_chars = val1.as_bytes();
        for val2 in vals2.clone() {
            let val2_chars = val2.as_bytes();
            if val1.len() != val2.len() {
                continue;
            }
            let mut diff_count = 0;
            let mut diff_index: usize = 0;
            for i in 0..val1.len() {
                if val1_chars[i] != val2_chars[i] {
                    diff_count += 1;
                    diff_index = i;
                }
            }
            if diff_count == 1 {
                println!("{} {}, index: {}", val1, val2, diff_index);
            }
        }
    }
}


struct Claim {
    id: i32,
    left: i32,
    top: i32,
    width: i32,
    height: i32,
}

fn day3(fname: &str) {
    let reader = BufReader::new(File::open(fname).unwrap());
    let mut claim_map = HashMap::new();
    let mut claims = Vec::new();

    let mut overlaps = 0;
    for line in reader.lines() {
        let claim = parse_claim(line.unwrap());
        for i in claim.left..claim.left + claim.width {
            for j in claim.top..claim.top + claim.height {
                claim_map.entry((i, j))
                    .and_modify(|val| {
                        println!("overlapped: x:{}, y:{}, val:{}", i, j, val);
                        if *val == 1 {
                            overlaps += 1;
                        }
                        *val = *val + 1;
                    })
                    .or_insert(1);
            }
        }
        claims.push(claim);
    }
    println!("{}", overlaps);
    for claim in claims {
        let mut score = 0;
        for i in claim.left..claim.left + claim.width {
            for j in claim.top..claim.top + claim.height {
                let entry = claim_map.get(&(i, j)).unwrap();
                score = score + entry;
            }
        }
        if score == claim.width * claim.height {
            println!("{} has no overlaps", claim.id);
        }
    }
}

fn parse_claim(line: String) -> Claim {
    // Example:
    // #1 @ 1,3: 4x4
    let mut parts = line.split_whitespace();
    let id = parts.next().unwrap().replace("#", "");
    let _ = parts.next(); // @
    let xy_str = parts.next().unwrap().to_string();
    let xy_str_stripped = xy_str.replace(":", "");
    let mut xy = xy_str_stripped.split(",");
    let wh_str = parts.next().unwrap();
    let mut wh = wh_str.split("x");

    Claim {
        id: id.parse::<i32>().unwrap(),
        left: xy.next().unwrap().parse::<i32>().unwrap(),
        top: xy.next().unwrap().parse::<i32>().unwrap(),
        width: wh.next().unwrap().parse::<i32>().unwrap(),
        height: wh.next().unwrap().parse::<i32>().unwrap(),
    }
}


fn day4(fname: &str) {
    let reader = BufReader::new(File::open(fname).unwrap());
    let mut lines = Vec::new();

    for line in reader.lines() {
        lines.push(line.unwrap());
    }
    lines.sort();

    /*
[1518-11-17 23:46] Guard #2399 begins shift
[1518-11-18 00:04] falls asleep
[1518-11-18 00:54] wakes up
*/
    let mut guard_tracker = HashMap::new();
    let mut last_guard_id = String::new();
    let mut last_sleep_min = 0;
    for line in lines {
        let parts: Vec<String> = line.split_whitespace().map(|s| s.to_string()).collect();

        let action: &str = parts.get(2).unwrap();
        match action {
            "Guard" => {
                last_guard_id = parts.get(3).unwrap().to_string();
            }
            "falls" => {
                let time_part: &str = parts.get(1).unwrap();
                last_sleep_min = time_part[3..5].parse().unwrap();
            }
            "wakes" => {
                let time_part: &str = parts.get(1).unwrap();
                let wake_min: i32 = time_part[3..5].parse().unwrap();
                let guard = guard_tracker.entry(last_guard_id.clone()).or_insert(GuardSleepSheet {
                    guard_id: last_guard_id.clone(),
                    total_sleep_minutes: 0,
                    minutes_histo: HashMap::new(),
                });
                guard.inc_minutes_in_range(last_sleep_min, wake_min);
            }
            _ => {
                panic!("unknown action {}", action);
            }
        }
    }
    let mut top_sleeper = &GuardSleepSheet {
        guard_id: String::new(),
        total_sleep_minutes: 0,
        minutes_histo: HashMap::new(),
    };

    for (_guard, sleep_sheet) in &guard_tracker {
        if sleep_sheet.total_sleep_minutes > top_sleeper.total_sleep_minutes {
            top_sleeper = sleep_sheet;
        }
    }
    let mut top_minute_count = 0;
    let mut top_minute = 0;
    for (min, count) in &top_sleeper.minutes_histo {
        if *count > top_minute_count {
            top_minute = *min;
            top_minute_count = *count;
        }
    }

    let mut guard_num: i32 = top_sleeper.guard_id.replace("#", "").parse().unwrap();
    let mut answer: i32 = guard_num * top_minute;

    println!("Part 1: Top guard {}, minute {}, answer {}", top_sleeper.guard_id, top_minute, answer);

    for (_guard, sleep_sheet) in &guard_tracker {
        for (min, count) in &sleep_sheet.minutes_histo {
            if *count > top_minute_count {
                top_minute = *min;
                top_minute_count = *count;
                top_sleeper = sleep_sheet;
            }
        }
    }

    guard_num = top_sleeper.guard_id.replace("#", "").parse().unwrap();
    answer = guard_num * top_minute;

    println!("Part 2: Top guard {}, minute {}, answer {}", top_sleeper.guard_id, top_minute, answer);
}

struct GuardSleepSheet {
    guard_id: String,
    total_sleep_minutes: i32,
    minutes_histo: HashMap<i32, i32>,
}

impl GuardSleepSheet {
    fn inc_minutes_in_range(&mut self, start: i32, end: i32) {
        for min in start..end {
            self.total_sleep_minutes += 1;
            self.minutes_histo.entry(min)
                .and_modify(|e| { *e += 1 })
                .or_insert(1);
        }
    }
}