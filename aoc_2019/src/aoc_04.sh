#!/bin/bash

echo "Day 4"

sort src/inputs/day4.txt | tr -d '[' | tr -d ']' | tr ':' ' ' |  \
awk '{if ($4=="Guard") { guard=$5 } else if ($4=="falls") {sleep_start=$3} else {for(i=sleep_start; i<$3;i++){print guard " " i}}}' | \
sort > /tmp/aoc_day4_stage_1.txt

# Part 1
SLEEPY_GUARD=`awk '{print $1}' /tmp/aoc_day4_stage_1.txt | uniq -c | sort -n | tail -1 | awk '{print $NF}' | tr -d '#'`
SLEEPY_TIME=`grep "#${SLEEPY_GUARD} " /tmp/aoc_day4_stage_1.txt | sort | uniq -c | sort -n | tail -1 | awk '{print $NF}'`
ANSWER=$((SLEEPY_GUARD*$SLEEPY_TIME))
echo "Part 1: Guard ${SLEEPY_GUARD} was asleep mostly at minute ${SLEEPY_TIME}. Answer: ${ANSWER}"

# Part 2
SLEEPY_LINE=`uniq -c /tmp/aoc_day4_stage_1.txt | sort -n | tail -1 | tr -d '#'`
read -r -a SLEEPY_PARTS <<< "${SLEEPY_LINE}"
SLEEPY_GUARD="${SLEEPY_PARTS[1]}"
SLEEPY_TIME="${SLEEPY_PARTS[2]}"
ANSWER=$((SLEEPY_GUARD*$SLEEPY_TIME))
echo "Part 2: Guard ${SLEEPY_GUARD} is most frequently asleep on same minute; that minute is ${SLEEPY_TIME}. Answer: ${ANSWER}"