#!/bin/bash

echo "Day 3"

sed 's/[#@,:x]/ /g' src/inputs/day3.txt | \
awk '{for(i=$2;i<$2+$4;i++){for(j=$3;j<$3+$5;j++){print $1 " " $4*$5 " " i ":" j}}}' | \
sort -k3 | uniq -c -f2 > /tmp/aoc_day3_stage1.txt

ANSWER=`awk '{print $1}' /tmp/aoc_day3_stage1.txt | egrep -v '^1$' | wc -l`
echo "Part 1 - Answer: ${ANSWER}"

ANSWER=`uniq -f2 /tmp/aoc_day3_stage1.txt | awk '{if($1=="1"){print $2 " " $3}}' | \
sort | uniq -c | awk '{if ($1==$3){print $2}}'`
echo "Part 2 - Answer: ${ANSWER}"
