#!/bin/bash

HAS_TWO=0
HAS_THREE=0
while read code; do
  CHAR_FREQS=`echo "$code" | grep -o . | sort | uniq -c | awk '{print $1}' | sort | uniq`
  HAS_TWO=$((HAS_TWO+=`echo -e "$CHAR_FREQS" | egrep '^2$' | wc -l`));
  HAS_THREE=$((HAS_THREE+=`echo -e "$CHAR_FREQS" | egrep '^3$' | wc -l`));
done <src/inputs/day2.txt

echo "Part 1 - Answer: $((HAS_TWO*HAS_THREE))"

ANSWER=""
check_barely_different() {
  a=$1
  b=$2
  if [ "${a}" == "${b}" -o "${#a}" != "${#b}" ]; then
    return
  fi
  DIFF_OFFSET=-1
  for (( i=0; i<${#a}; i++ )); do
    if [ "${a:$i:1}" != "${b:$i:1}" ]; then
      if [ $DIFF_OFFSET != -1 ]; then
        return
      fi
      DIFF_OFFSET=$i
    fi
  done
  ANSWER="${a:0:$DIFF_OFFSET}${a:$((DIFF_OFFSET+1))}"
}

while read code_outer; do
  while read code_inner; do
    check_barely_different "${code_outer}" "${code_inner}"
    if [ ! -z "${ANSWER}" ]; then
      echo "Part 2 - Answer: ${ANSWER}"
      exit 0
    fi
  done <src/inputs/day2.txt
done <src/inputs/day2.txt
