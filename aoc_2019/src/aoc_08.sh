#!/bin/bash

STACK=()
STACK_POS=0
STACK_CURRENT=""

stack_push() {
  STACK[$STACK_POS]=$1
  STACK_POS=$((STACK_POS+1))
}

stack_pop() {
  STACK_POS=$((STACK_POS-1))
  STACK_CURRENT="${STACK[$STACK_POS]}"
}

CONTENT=($(<src/inputs/day8.txt))
CONTENT_POS=0
CONTENT_CURRENT=""

next_number() {
  CONTENT_CURRENT="${CONTENT[$CONTENT_POS]}"
  CONTENT_POS=$((CONTENT_POS+1))
}

META_SUM=0
node_scores=()
node_child_counts=()
node_first_child=()
node_stack=()
node_pos=-1

NEXT_NODE_ID=1

stack_push "0"

while [ ${STACK_POS} -gt 0 ]; do
  stack_pop

  if [ "${STACK_CURRENT:0:1}" == "m" ]; then
    # metadata operation. eg "m7" means pull 7 metadata values from the stream

    CURR_NODE_ID="${node_stack[$node_pos]}"
    max_children="${node_child_counts[$CURR_NODE_ID]}"

    # bash-ism: ${FOO'#*m'} means remove the prefix up to and including the last 'm' in the FOO variable
    meta_count="${STACK_CURRENT#*m}"

    for ((i=0;i<$meta_count;i++)); do
      next_number
      meta_val=${CONTENT_CURRENT}
      META_SUM=$((META_SUM+meta_val))
      if [ $max_children == 0 ]; then
        curr_score="${node_scores[$CURR_NODE_ID]}"
        node_scores[$CURR_NODE_ID]=$((curr_score+meta_val))
      elif (( $meta_val <= $max_children )); then
        first_child="${node_first_child[$CURR_NODE_ID]}"
        child_id=$(( first_child + (meta_val-1) ))
        child_score="${node_scores[$child_id]}"

        curr_score="${node_scores[$CURR_NODE_ID]}"
        node_scores[$CURR_NODE_ID]=$((curr_score+child_score))
      fi
    done

    # when done reading meta (even "m0"!) we are done with handling some node, pop it off the node stack
    node_pos=$((node_pos-1))
  else
    # node operation. pull a header from the stream. the value of the STACK_CURRENT is the node id the
    # header will apply to
    node_id=${STACK_CURRENT}
    node_pos=$((node_pos+1))
    node_stack[$node_pos]=$node_id

    next_number
    child_count=$CONTENT_CURRENT
    next_number
    meta_count=$CONTENT_CURRENT

    stack_push "m${meta_count}"

    node_child_counts[$node_id]=$child_count

    if (( child_count > 0 )); then
      first_child=$NEXT_NODE_ID
      node_first_child[$node_id]=$NEXT_NODE_ID
      NEXT_NODE_ID=$((NEXT_NODE_ID+child_count))
      for ((i=1;i<=$child_count;i++)); do
        stack_push $((NEXT_NODE_ID-i))
      done
    fi
  fi
done
echo "Part 1 - Answer: ${META_SUM}"
echo "Part 2 - Answer: ${node_scores[0]}"