use std::env;
use std::time::Instant;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::collections::{HashSet, HashMap};
use regex::Regex;

// call as:
// ./aoc_2020 [day].[part] verbose
// where "verbose" is optional. eg:
// ./aoc_2020 1.2
// ./aoc_2020 1.2 verbose
fn main() {
    let args: Vec<String> = env::args().collect();
    let problem = args.get(1).unwrap().to_string();
    let mut verbose = false;
    if args.len() > 2 && args.get(2).unwrap().to_string() == "verbose" {
        verbose = true;
    }

    let answer;
    let before = Instant::now();
    match problem.as_ref() {
        "1.1" => {
            answer = day1_part1("src/inputs/1.1.txt", verbose);
        },
        "1.2" => {
            answer = day1_part2("src/inputs/1.1.txt", verbose)
        },
        "2.1" => {
            answer = day2_part1("src/inputs/2.1.txt", verbose)
        },
        "2.2" => {
            answer = day2_part2("src/inputs/2.1.txt", verbose)
        },
        "2.2m" => {
            answer = day2_part2_mapped("src/inputs/2.1.txt", verbose)
        },
        "3.1e" => {
            answer = day3_part1("src/inputs/3.1-ex.txt", verbose)
        },
        "3.1" => {
            answer = day3_part1("src/inputs/3.1.txt", verbose)
        },
        "3.2e" => {
            answer = day3_part2("src/inputs/3.1-ex.txt", verbose)
        },
        "3.2" => {
            answer = day3_part2("src/inputs/3.1.txt", verbose)
        },
        "4.1e" => {
            answer = day4_part1("src/inputs/4.1-ex.txt", verbose)
        },
        "4.1" => {
            answer = day4_part1("src/inputs/4.1.txt", verbose)
        },
        "4.2" => {
            answer = day4_part2("src/inputs/4.1.txt", verbose)
        },
        "5.1e" => {
            answer = day5_part1("src/inputs/5.1-ex.txt", verbose)
        },
        "5.1" => {
            answer = day5_part1("src/inputs/5.1.txt", verbose)
        },
        "5.2" => {
            answer = day5_part2("src/inputs/5.1.txt", verbose)
        },
        "6.1" => {
            answer = day6_part1("src/inputs/6.1.txt", verbose)
        },
        "6.2" => {
            answer = day6_part2("src/inputs/6.1.txt", verbose)
        },
        "7.1e" => {
            answer = day7_part1("src/inputs/7.1-ex.txt", verbose)
        },
        "7.1" => {
            answer = day7_part1("src/inputs/7.1.txt", verbose)
        },
        "7.2e" => {
            answer = day7_part2("src/inputs/7.2-ex.txt", verbose)
        },
        "7.2" => {
            answer = day7_part2("src/inputs/7.1.txt", verbose)
        },
        "8.1e" => {
            answer = day8_part1("src/inputs/8.1-ex.txt", verbose)
        },
        "8.1" => {
            answer = day8_part1("src/inputs/8.1.txt", verbose)
        },
        "8.2" => {
            answer = day8_part2("src/inputs/8.1.txt", verbose)
        },
        "9.1e" => {
            answer = day9_part1("src/inputs/9.1-ex.txt", verbose, 5)
        },
        "9.1" => {
            answer = day9_part1("src/inputs/9.1.txt", verbose, 25)
        },
        "9.2e" => {
            answer = day9_part2("src/inputs/9.1-ex.txt", verbose, 5)
        },
        "9.2" => {
            answer = day9_part2("src/inputs/9.1.txt", verbose, 25)
        },
        "10.1e" => {
            answer = day10_part1("src/inputs/10.1-ex.txt", verbose)
        },
        "10.1" => {
            answer = day10_part1("src/inputs/10.1.txt", verbose)
        },
        "10.2e" => {
            answer = day10_part2("src/inputs/10.1-ex.txt", verbose)
        },
        "10.2" => {
            answer = day10_part2("src/inputs/10.1.txt", verbose)
        },
        "11.1e" => {
            answer = day11_part1("src/inputs/11.1-ex.txt", verbose)
        },
        "11.1" => {
            answer = day11_part1("src/inputs/11.1.txt", verbose)
        },
        "11.2e" => {
            answer = day11_part2("src/inputs/11.1-ex.txt", verbose)
        },
        "11.2" => {
            answer = day11_part2("src/inputs/11.1.txt", verbose)
        },
        "12.1e" => {
            answer = day12_part1("src/inputs/12.1-ex.txt", verbose)
        },
        "12.1" => {
            answer = day12_part1("src/inputs/12.1.txt", verbose)
        },
        "12.2e" => {
            answer = day12_part2("src/inputs/12.1-ex.txt", verbose)
        },
        "12.2" => {
            answer = day12_part2("src/inputs/12.1.txt", verbose)
        },
        "13.1e" => {
            answer = day13_part1("src/inputs/13.1-ex.txt", verbose)
        },
        "13.1" => {
            answer = day13_part1("src/inputs/13.1.txt", verbose)
        },
        "13.2e" => {
            answer = day13_part2("src/inputs/13.1-ex.txt", verbose)
        },
        "13.2" => {
            answer = day13_part2("src/inputs/13.1.txt", verbose)
        },
        "14.1e" => {
            answer = day14_part1("src/inputs/14.1-ex.txt", verbose)
        },
        "14.1" => {
            answer = day14_part1("src/inputs/14.1.txt", verbose)
        },
        "14.2e" => {
            answer = day14_part2("src/inputs/14.2-ex.txt", verbose)
        },
        "14.2" => {
            answer = day14_part2("src/inputs/14.1.txt", verbose)
        },
        "15.1e" => {
            answer = day15_part1("src/inputs/15.1-ex.txt", verbose)
        },
        "15.1" => {
            answer = day15_part1("src/inputs/15.1.txt", verbose)
        },
        "15.2e" => {
            answer = day15_part2("src/inputs/15.1-ex.txt", verbose)
        },
        "15.2" => {
            answer = day15_part2("src/inputs/15.1.txt", verbose)
        },
        "16.1e" => {
            answer = day16_part1("src/inputs/16.1-ex.txt", verbose)
        },
        "16.1" => {
            answer = day16_part1("src/inputs/16.1.txt", verbose)
        },
        "16.2e" => {
            answer = day16_part2("src/inputs/16.2-ex.txt", verbose)
        },
        "16.2" => {
            answer = day16_part2("src/inputs/16.1.txt", verbose)
        },
        "17.1e" => {
            answer = day17_part1("src/inputs/17.1-ex.txt", verbose)
        },
        "17.1" => {
            answer = day17_part1("src/inputs/17.1.txt", verbose)
        },
        "17.2e" => {
            answer = day17_part2("src/inputs/17.1-ex.txt", verbose)
        },
        "17.2" => {
            answer = day17_part2("src/inputs/17.1.txt", verbose)
        },
        "18.1e" => {
            answer = day18_part1("src/inputs/18.1-ex.txt", verbose)
        },
        "18.1" => {
            answer = day18_part1("src/inputs/18.1.txt", verbose)
        },
        "18.2e" => {
            answer = day18_part2("src/inputs/18.1-ex.txt", verbose)
        },
        "18.2" => {
            answer = day18_part2("src/inputs/18.1.txt", verbose)
        },
        "19.1e" => {
            answer = day19_part1("src/inputs/19.1-ex.txt", verbose)
        },
        "19.1" => {
            answer = day19_part1("src/inputs/19.1.txt", verbose)
        },
        "19.2e" => {
            answer = day19_part1("src/inputs/19.2-ex.txt", verbose)
        },
        "19.2" => {
            answer = day19_part1("src/inputs/19.2.txt", verbose)
        },
        "20.1e" => {
            answer = day20_part1("src/inputs/20.1-ex.txt", verbose)
        },
        "20.1" => {
            answer = day20_part1("src/inputs/20.1.txt", verbose)
        },
        "20.2e" => {
            answer = day20_part2("src/inputs/20.1-ex.txt", verbose)
        },
        "20.2" => {
            answer = day20_part2("src/inputs/20.1.txt", verbose)
        },
        "21.1e" => {
            answer = day21_part1("src/inputs/21.1-ex.txt", verbose)
        },
        "21.1" => {
            answer = day21_part1("src/inputs/21.1.txt", verbose)
        },
        "22.1e" => {
            answer = day22_part1("src/inputs/22.1-ex.txt", verbose)
        },
        "22.1" => {
            answer = day22_part1("src/inputs/22.1.txt", verbose)
        },
        "22.2e" => {
            answer = day22_part2("src/inputs/22.1-ex.txt", verbose)
        },
        "22.2" => {
            answer = day22_part2("src/inputs/22.1.txt", verbose)
        },
        "23.1e" => {
            answer = day23_part2("389125467", 9, verbose)
        },
        "23.1" => {
            answer = day23_part2("394618527", 9, verbose)
        },
        "23.2e" => {
            answer = day23_part2("389125467", 1_000_000, verbose)
        },
        "23.2" => {
            answer = day23_part2("394618527", 1_000_000, verbose)
        },
        "24.1e" => {
            answer = day24_part1("src/inputs/24.1-ex.txt", verbose)
        },
        "24.1" => {
            answer = day24_part1("src/inputs/24.1.txt", verbose)
        },
        "24.2e" => {
            answer = day24_part2("src/inputs/24.1-ex.txt", verbose)
        },
        "24.2" => {
            answer = day24_part2("src/inputs/24.1.txt", verbose)
        },
        "25.1e" => {
            answer = day25_part1("src/inputs/25.1-ex.txt", verbose)
        },
        "25.1" => {
            answer = day25_part1("src/inputs/25.1.txt", verbose)
        },
        _ => panic!("unknown problem {}", problem)
    }
    let elapsed = before.elapsed();
    println!("Answer for problem {} is {}\nElapsed time: {:.2?}", problem, answer, elapsed);
}


fn day25_part1(fname: &str, verbose: bool) -> usize {
    let lines: Vec<String> = BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok).collect();

    if lines.len() != 2 {
        panic!("needs 2 public keys in input");
    }
    
    let pub_key_1 = lines[0].parse::<usize>().unwrap();
    let loop_key_1 = determine_loop_size_for(pub_key_1);

    let pub_key_2 = lines[1].parse::<usize>().unwrap();
    let loop_key_2 = determine_loop_size_for(pub_key_2);

    let enc_key_1 = encrypt_value(pub_key_2, loop_key_1);
    let enc_key_2 = encrypt_value(pub_key_1, loop_key_2);

    if verbose {
        println!("enc 1: {}, enc 2: {}", enc_key_1, enc_key_2);
    }

    return 0;
}

fn encrypt_value(subject_number: usize, loops: usize) -> usize {
    let mut value = 1;
    for _i in 0..loops {
        value *= subject_number;
        value %= 20201227;
    }
    return value;
}

fn determine_loop_size_for(key: usize) -> usize {
    let mut value = 1;
    let mut i = 0; 
    loop {
        value *= 7;
        value %= 20201227;
        i += 1;
        if value == key {
            return i;
        }
    }
}

fn day24_part2(fname: &str, verbose: bool) -> usize {
    let mut black_coords: HashSet<(isize, isize)> = HashSet::new();

    for line in BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok) {
        let directions = parse_directions(&line);
        let coord = resolve_directions(directions, verbose);
        if black_coords.contains(&coord) {
            black_coords.remove(&coord);
            if verbose {
                println!("flipped back to white {:?}", &coord);
            }
        } else {
            black_coords.insert(coord);
            if verbose {
                println!("flipped to black {:?}", &coord);
            }
        }
    }

    for _i in 0..100 {
        black_coords = flip_floor_tiles(black_coords, verbose);
    }

    return black_coords.len();
}

fn flip_floor_tiles(black_coords: HashSet<(isize, isize)>, verbose: bool) -> HashSet<(isize, isize)> {
    let mut checked: HashSet<(isize, isize)> = HashSet::new();
    let mut updated: HashSet<(isize, isize)> = HashSet::new();

    for tile in &black_coords {
        let neighbors = find_tile_neighbors(*tile);
        let b_count = neighbors.iter().filter(|nt| black_coords.contains(nt)).count();
        if b_count != 0 && b_count <= 2 {
            updated.insert(*tile);
        }
        if verbose {
            println!("neighbors of {:?} are {:?}, {} are black tiles.", tile, neighbors, b_count);
        }
        for n in neighbors.iter().filter(|n| !black_coords.contains(n)) {
            if checked.contains(n) {
                continue;
            }
            let sub_neighbors = find_tile_neighbors(*n);
            let snb_count = sub_neighbors.iter().filter(|nt| black_coords.contains(nt)).count();
            if snb_count == 2 {
                updated.insert(*n);
            }
            checked.insert(*n);
        }
    }
    return updated;
}

fn find_tile_neighbors(tile: (isize, isize)) -> Vec<(isize, isize)> {
    let mut neighbors: Vec<(isize, isize)> = Vec::new();
    for dir in &[(2, 0), (1, 1), (1, -1), (-2, 0), (-1, -1), (-1, 1)] {
        neighbors.push((tile.0 + dir.0, tile.1 + dir.1));
    }

    return neighbors;
}

fn day24_part1(fname: &str, verbose: bool) -> usize {
    let mut black_coords: HashSet<(isize, isize)> = HashSet::new();

    for line in BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok) {
        let directions = parse_directions(&line);
        let coord = resolve_directions(directions, verbose);
        if black_coords.contains(&coord) {
            black_coords.remove(&coord);
            if verbose {
                println!("flipped back to white {:?}", &coord);
            }
        } else {
            black_coords.insert(coord);
            if verbose {
                println!("flipped to black {:?}", &coord);
            }
        }
    }

    return black_coords.len();
}

#[derive(Debug)]
enum HexDir {
    East,
    SouthEast,
    SouthWest,
    West,
    NorthWest,
    NorthEast,
}

fn parse_directions(line: &str) -> Vec<HexDir> {
    use HexDir::*;

    let mut directions: Vec<HexDir> = Vec::new();
    let chars: Vec<char> = line.chars().collect();

    let mut i = 0;
    while i < chars.len() {
        let dir = match chars[i] {
            'e' => East,
            'w' => West,
            'n' => {
                i += 1;
                match chars[i] {
                    'e' => NorthEast,
                    'w' => NorthWest,
                    _ => panic!("bad"),
                }
            },
            's' => {
                i += 1;
                match chars[i] {
                    'e' => SouthEast,
                    'w' => SouthWest,
                    _ => panic!("bad"),
                }
            },
            _ => panic!("bad"),
        };
        directions.push(dir);
        i+=1;
    }
    return directions;
}

fn resolve_directions(directions: Vec<HexDir>, verbose: bool) -> (isize, isize) {
    let mut curr: (isize, isize) = (0, 0);

    use HexDir::*;
    for dir in directions {
        let next = match &dir {
            East => (curr.0 + 2, curr.1),
            SouthEast => (curr.0 + 1, curr.1 - 1),
            SouthWest => (curr.0 - 1, curr.1 - 1),
            West => (curr.0 - 2, curr.1),
            NorthWest => (curr.0 - 1, curr.1 + 1),
            NorthEast => (curr.0 + 1, curr.1 + 1),
        };
        if verbose {
            println!("from {:?}, going {:?}, lands at {:?}", curr, &dir, next);
        }
        curr = next;
    }

    return curr;
}

fn day23_part2(input: &str, max_val: usize, verbose: bool) -> usize {
    let starting_cups: Vec<usize> = input.chars().map(|c| c.to_digit(10)).filter_map(|d| d).map(|d| d as usize).collect();

    let mut cups: Vec<usize> = Vec::with_capacity(max_val+1);
    cups.resize(max_val+1, 0);

    let mut prev = starting_cups[0];
    for i in 1..starting_cups.len() {
        if verbose {
            println!("[{}] => {}", prev, starting_cups[i]);
        }
        cups[prev] = starting_cups[i];
        prev = starting_cups[i];
    }

    for i in starting_cups.len()+1..=max_val {
        if cups[prev] == 0 {
            if verbose {
                println!("[{}] => {} !!", prev, i);
            }
            cups[prev] = i;
        }
        prev = i;
    }

    let mut curr_val = starting_cups[0];

    cups[prev] = curr_val;
    
    for i in 0..10_000_000 {
        let mid = shell_game_round_v2(cups, curr_val, max_val, i+1, verbose);
        cups = mid.0;
        curr_val = mid.1;
    }

    return shell_game_status_part2(&cups);
}

fn shell_game_status_part2(cups: &Vec<usize>) -> usize {
    let first = cups[1];
    let second = cups[first];
    return first * second;
}

fn shell_game_round_v2(mut cups: Vec<usize>, curr_val: usize, max_val: usize, round: usize, verbose: bool) -> (Vec<usize>, usize) {

    if verbose {
        println!("Before round {}: cups: {:?}, flattened: {:?}", round, &cups, flatten_cups(&cups, curr_val));
    }
    let next_val = cups[curr_val];
    let next_val_2 = cups[next_val];
    let next_val_3 = cups[next_val_2];

    cups[curr_val] = cups[next_val_3];

    let mut insert_after = curr_val;
    while curr_val == insert_after || next_val == insert_after || next_val_2 == insert_after || next_val_3 == insert_after {
        insert_after = match insert_after {
            1 => max_val,
            _ => insert_after - 1,
        };
    }

    let curr_insert_val = cups[insert_after];
    cups[insert_after] = next_val;
    cups[next_val_3] = curr_insert_val;

    let ret_val = cups[curr_val];
    if verbose {
        println!("after round: {}, flat cups: {:?}, curr: {}, removed: {}, {}, {}, insert target value: {}", round, flatten_cups(&cups, ret_val), curr_val, next_val, next_val_2, next_val_3, insert_after);
    }
    return (cups, ret_val);
}

fn flatten_cups(cups: &Vec<usize>, curr_val: usize) -> Vec<usize> {
    let mut flattened: Vec<usize> = Vec::new();
    flattened.push(curr_val);
    let mut next_val = cups[curr_val];
    while next_val != curr_val {
        flattened.push(next_val);
        next_val = cups[next_val];
    }

    return flattened;
}

fn day22_part2(fname: &str, verbose: bool) -> usize {
    let mut hands: Vec<Vec<usize>> = Vec::new();

    for line in BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok) {
        if line.starts_with("Player") {
            hands.push(Vec::new());
        } else if line.len() == 0 {
            // skip
        } else {
            let i = hands.len()-1;
            hands.get_mut(i).unwrap().push(line.parse::<usize>().unwrap());
        }
    }

    let mut seen: HashSet<String> = HashSet::new();
    let res: (usize, Vec<Vec<usize>>) = play_hi_low(hands, &mut seen, verbose);

    return calc_hi_low_scores(&res.1)[res.0];
}

fn calc_hi_low_scores_key(hands: &Vec<Vec<usize>>) -> String {
    hands.iter().map(|hand| hand.iter().map(|card| card.to_string()).collect::<Vec<String>>().join(",")).collect::<Vec<String>>().join("|").to_string()
}

fn calc_hi_low_scores(hands: &Vec<Vec<usize>>) -> Vec<usize> {
    let mut scores: Vec<usize> = Vec::new();
    for hand in hands {
        scores.push(calc_hi_low_score(&hand));
    }
    return scores;
}

fn calc_hi_low_score(hand: &Vec<usize>) -> usize {
    let mut score = 0;
    for i in 0..hand.len() {
        let rank = hand.len() - i;
        score += rank * hand[i];
    }
    return score;
}

fn play_hi_low(mut hands: Vec<Vec<usize>>, seen: &mut HashSet<String>, verbose: bool) -> (usize, Vec<Vec<usize>>) {
    let mut round = 0;
    loop {
        if let Some(winner) = determine_winner(&hands) {
            return (winner, hands);
        }
        round += 1;
        if verbose {
            println!("before round {}", round);
            for hand in &hands {
                println!("{:?}", hand);
            }
        }

        let mut maybe_round_winner_index: Option<usize> = None;

        let state_key = calc_hi_low_scores_key(&hands);
        
        if seen.contains(&state_key) {
            if verbose {
                println!("state already seen! player 1 wins.");
            }
            return (0, hands);
        }
        if verbose {
            println!("key: {:?}", &state_key);
        }
        seen.insert(state_key);

        for i in 0..hands.len() {
            if hands[i].len() == 0 {
                continue;
            }
            match maybe_round_winner_index {
                None => {
                    maybe_round_winner_index = Some(i);
                },
                Some(index) => {
                    // determine recursive state (values returned here are >= the len), if so, play_hi_low, and the winner is returned.
                    if hands[i][0] <= (hands[i].len() - 1) && hands[index][0] <= (hands[index].len() - 1) {
                        let res: (usize, _) = play_hi_low(split_hands(&hands), &mut seen.clone(), verbose);
                        maybe_round_winner_index = Some(res.0);
                    } else {
                        if hands[i][0] > hands[index][0] {
                            maybe_round_winner_index = Some(i);
                        }
                    }
                },
            }
        }

        match maybe_round_winner_index {
            None => {
                panic!("round didn't have a winner.");
            },
            _ => (),
        }

        let round_winner_index = maybe_round_winner_index.unwrap();
        let win_val = hands[round_winner_index][0];
        hands[round_winner_index].push(win_val);
        for i in 0..hands.len() {
            if i == round_winner_index || hands[i].len() == 0 {
                continue;
            }
            let val = hands[i][0];
            hands[round_winner_index].push(val);
        }

        for i in 0..hands.len() {
            if verbose {
                println!("shifting index {} from {:?} to {:?}", i, hands[i], hands[i][1..].to_vec());
            }
            if hands[i].len() > 0 {
                hands[i] = hands[i][1..].to_vec();
            }
        }
    }
}

fn split_hands(hands: &Vec<Vec<usize>>) -> Vec<Vec<usize>> {
    let mut split: Vec<Vec<usize>> = Vec::new();
    for hand in hands {
        split.push(hand[1..=hand[0]].to_vec());
    }
    return split;
}


fn determine_winner(decks: &Vec<Vec<usize>>) -> Option<usize> {
    let mut remaining: Vec<usize> = Vec::new();
    for i in 0..decks.len() {
        if decks[i].len() > 0 {
            remaining.push(i);
        }
    }
    if remaining.len() > 1 {
        return None;
    }
    return Some(remaining[0]);
}

fn day22_part1(fname: &str, verbose: bool) -> usize {
    let mut hands: Vec<Vec<usize>> = Vec::new();

    for line in BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok) {
        if line.starts_with("Player") {
            hands.push(Vec::new());
        } else if line.len() == 0 {
            // skip
        } else {
            let i = hands.len()-1;
            hands.get_mut(i).unwrap().push(line.parse::<usize>().unwrap());
        }
    }

    let mut round = 0;
    while hands.len() > 1 {
        round += 1;
        if verbose {
            println!("before round {}", round);
            for hand in &hands {
                println!("{:?}", &hand);
            }
        }
    
        let mut high_index = 0;
        let mut values: Vec<usize> = Vec::new();
        values.push(hands[0][0]);
        for i in 1..hands.len() {
            values.push(hands[i][0]);
            if hands[i][0] > hands[high_index][0] {
                high_index = i;
            }
        }
        values.sort();
        values = values.into_iter().rev().collect();

        if verbose {
            println!("sorted values for round: {:?}, high_index: {}, hands.len(): {}", &values, high_index, &hands.len());
        }
        for val in values {
            hands[high_index].push(val);
        }
        for i in (0..hands.len()).rev() {
            if verbose {
                println!("shifting index {} from {:?} to {:?}", i, hands[i], hands[i][1..].to_vec());
            }
            if hands[i].len() == 1 {
                hands.remove(i);
            } else {
                hands[i] = hands[i][1..].to_vec();
            }
        }
    }

    let mut score = 0;

    for i in 0..hands[0].len() {
        let rank = hands[0].len() - i;
        score += rank * hands[0][i];
    }

    return score;
}



fn day21_part1(fname: &str, verbose: bool) -> usize {
    let mut ingredients: HashMap<String, usize> = HashMap::new();
    let mut allergens: HashMap<String, HashSet<String>> = HashMap::new();

    for line in BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok) {
        let parts: Vec<&str> = line.split(" (").collect();
        if parts.len() != 2 {
            panic!("ingredients list doesn't have allergens.");
        }
        let line_ingredients: HashSet<String> = parts[0].split(' ').filter(|value| value.len() > 0).map(|value| value.to_string()).collect();
        let line_allergens: HashSet<String> = parts[1][..parts[1].len()-1].split(',').map(|value| value.replace("contains", "")).map(|value| value.trim().to_string()).collect();

        for ing in &line_ingredients {
            let curr_count = ingredients.entry(ing.to_string()).or_insert(0);
            *curr_count += 1;
        }
        for allergen in &line_allergens {
            let merged: HashSet<String> = match allergens.get(allergen) {
                Some(curr) => {
                    curr.intersection(&line_ingredients).cloned().collect()
                },
                None => {
                    line_ingredients.clone()
                },
            };
            allergens.insert(allergen.to_string(), merged);
        }

        let mut solos: HashSet<String> = allergens.iter().filter(|(_a, i)| i.len() == 1).map(|(_a, i)| i.iter().next().unwrap()).map(|value| value.to_string()).collect();
        let mut solo_diff = solos.len();
        while solo_diff > 0 {
            for (_a, i) in allergens.iter_mut() {
                if i.len() == 1 {
                    continue;
                }
                for solo in &solos {
                    (*i).remove(solo);
                }
            }
            let next_solos: HashSet<String> = allergens.iter().filter(|(_a, i)| i.len() == 1).map(|(_a, i)| i.iter().next().unwrap()).map(|value| value.to_string()).collect();
            solo_diff = next_solos.len() - solos.len();
            solos = next_solos;
        }
    }

    if verbose {
        println!("{:?} => {:?}", &ingredients, &allergens);
    }

    let mut sum: usize = 0;
    for (i, c) in ingredients {
        if allergens.iter().filter(|(_a, ai)| ai.contains(&i)).count() == 0 {
            sum += c;
        }
    }

    let mut allergen_names: Vec<String> = allergens.iter().filter(|(_a, i)| i.len() == 1).map(|(a, _i)| a.to_string()).collect();
    allergen_names.sort();

    let mut dangers: Vec<String> = Vec::new();

    for a in allergen_names {
        dangers.push(allergens.get(&a).unwrap().iter().next().unwrap().to_string());
    }
    println!("{}", dangers.join(","));
    return sum;
}

fn day20_part2(fname: &str, verbose: bool) -> usize {
    let mut tiles: HashMap<u128, Vec<Vec<char>>> = HashMap::new();

    let mut curr_tile_id = 0;
    let mut tile_buffer: Vec<Vec<char>> = Vec::new();
    for line in BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok) {
        if line.len() == 0 {
            tiles.insert(curr_tile_id, tile_buffer);
            curr_tile_id = 0;
            tile_buffer = Vec::new();
            continue;
        }
        if curr_tile_id == 0 {
            let parts: Vec<String> = line.split(' ').map(|val| val.to_string()).collect();
            let id_part = parts[1].to_string();
            let end = id_part.len() - 1;
            if verbose {
                println!("split on {} -> {:?}", line, parts);
            }
            curr_tile_id = id_part[..end].parse::<u128>().unwrap();
            continue;
        }
        let chars: Vec<char> = line.chars().collect();
        tile_buffer.push(chars);
    }
    if tile_buffer.len() > 0 {
        tiles.insert(curr_tile_id, tile_buffer);
    }

    let edge_index = build_edge_index(&tiles);

    let mut corners: Vec<u128> = Vec::new();
    for (k, _v) in &tiles {
        if count_edges(&tiles, *k, &edge_index, verbose) == 4 {
            corners.push(*k);
        }
    }

    let mut assembled: Vec<Vec<u128>> = Vec::new();
    assembled.push(vec![corners[0]]);

    // orient first corner to be top left (no neighbor on top nor left)
    let mut neighbors = tile_neighbors(&tiles, corners[0], &edge_index);
    while neighbors[0] > 0 || neighbors[2] > 0 {
        tiles.insert(corners[0], rotate(tiles.get(&corners[0]).unwrap()));
        neighbors = tile_neighbors(&tiles, corners[0], &edge_index);
    }

    let mut curr_row_index = Some(0);
    let mut last_tile_id = corners[0];
    while let Some(row_index) = curr_row_index {
        while let Some((neighbor, oriented)) = find_right_neighbor(&tiles, last_tile_id, &edge_index, verbose) {
            assembled[row_index].push(neighbor);
            last_tile_id = neighbor;
            tiles.insert(neighbor, oriented);
            print_tiles(&tiles, &assembled, verbose);
        }
        if verbose {
            println!("done with row {}", row_index);
            if verbose {
                for tile_row in &assembled {
                    println!("{:?}", tile_row);
                }
            }        
        }
        if verbose {
            println!("dropping down to next row.");
        }

        let first_tile_id = assembled[row_index][0];
        if let Some((bottom_neighbor, oriented)) = find_bottom_neighbor(&tiles, first_tile_id, &edge_index, verbose) {
            last_tile_id = bottom_neighbor;
            let bn_row = vec![bottom_neighbor];
            assembled.push(bn_row);
            curr_row_index = Some(row_index + 1);
            tiles.insert(bottom_neighbor, oriented);
        } else {
            curr_row_index = None;
        }
    }

    if verbose {
        for tile_row in &assembled {
            println!("{:?}", tile_row);
        }
    }

    let mut image = assemble_image(&tiles, &assembled);

    if verbose {
        println!("image:");
        print_tile(&image);
    }

    let dragon_mask: Vec<u128> = vec![
        char_line_to_bitset(&"                  # ".chars().collect()),                             
        char_line_to_bitset(&"#    ##    ##    ###".chars().collect()),
        char_line_to_bitset(&" #  #  #  #  #  #   ".chars().collect()),
    ];
    let dragon_width = 20;
    let line_width = image[0].len();

    let mut max_dragons = 0;
    // (flip t-b, flip l-r)
    for flips in &[(false, false), (false, true), (true, false), (true, true)] {
        match flips {
            (false, false) => (),
            (false, true) => {
                image = flip_left_right(&image);
            },
            (true, false) => {
                image = rotate_right(&image);
                image = flip_left_right(&image);
                image = rotate_right(&image);
                image = rotate_right(&image);
                image = rotate_right(&image);
            },
            (true, true) => {
                image = flip_left_right(&image);
                image = rotate_right(&image);
                image = flip_left_right(&image);
                image = rotate_right(&image);
                image = rotate_right(&image);
                image = rotate_right(&image);
            },
        }
        for _r in 0..4 { // calc for all 4 rotations
            let mut dragon_count = 0;
            for i in 0..image.len()-dragon_mask.len() {
                for s in 0..(line_width - dragon_width) { // calc for each dragon width.
                    let mut matches = 0;
                    for j in 0..dragon_mask.len() {
                        let encoded = char_line_to_bitset(&image[i+j]);
                        let shifty_dragon = dragon_mask[j] << s;
                        if (encoded & shifty_dragon) == shifty_dragon {
                            matches += 1;
                        }
                    }
                    if matches == dragon_mask.len() {
                        dragon_count += 1;
                        if verbose {
                            println!("found dragon on line {} with {} shifts", i, s);
                            print_tile(&image[i..i+dragon_mask.len()].to_vec());
                        }
                    }
                }
            }
            if verbose {
                println!("found {} dragons", dragon_count);
            }
            if dragon_count > max_dragons {
                max_dragons = dragon_count;
            }
            image = rotate(&image);
        }
        match flips { // flip back to original
            (false, false) => (),
            (false, true) => {
                image = flip_left_right(&image);
            },
            (true, false) => {
                image = rotate_right(&image);
                image = flip_left_right(&image);
                image = rotate_right(&image);
                image = rotate_right(&image);
                image = rotate_right(&image);
            },
            (true, true) => {
                image = flip_left_right(&image);
                image = rotate_right(&image);
                image = flip_left_right(&image);
                image = rotate_right(&image);
                image = rotate_right(&image);
                image = rotate_right(&image);
            },
        }
    }
    let mut dragon_hash_count = 0;
    for dm in dragon_mask {
        dragon_hash_count += dm.count_ones();
    }

    dragon_hash_count *= max_dragons;

    let mut all_hash_count = 0;
    for line in image {
        let encoded = char_line_to_bitset(&line);
        let one_bit_count = encoded.count_ones();
        let hash_count = count_hashes(&line);
        if one_bit_count != hash_count as u32 {
            println!("got different counts for {:?}, {} vs {}", &line, one_bit_count, hash_count);
        }
        all_hash_count += hash_count;
    }

    if verbose {
        println!("found {} dragons ({}/{} hashes)", max_dragons, dragon_hash_count, all_hash_count);
    }

    return (all_hash_count - dragon_hash_count as usize) as usize;
}

fn count_hashes(line: &Vec<char>) -> usize {
    line.iter().filter(|c| **c == '#').count()
}

fn assemble_image(tiles: &HashMap<u128, Vec<Vec<char>>>, coords: &Vec<Vec<u128>>) -> Vec<Vec<char>> {
    let mut dechromed: HashMap<u128, Vec<Vec<char>>> = HashMap::new();
    for (k, v) in tiles {
        dechromed.insert(*k, dechrome_tile(v));
    }

    let mut image: Vec<Vec<char>> = Vec::new();

    for tile_row in coords {
        for i in 0..dechromed.get(&tile_row[0]).unwrap().len() {
            let mut row: Vec<char> = Vec::new();

            for tile_id in tile_row {
                row.extend(dechromed.get(&tile_id).unwrap()[i].to_vec());
            }
            image.push(row);
        }
    }

    return image;
}

fn dechrome_tile(tile: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut res: Vec<Vec<char>> = Vec::new();

    for i in 1..&tile.len()-1 {
        let row = &tile[i];
        res.push(row[1..row.len()-1].to_vec());
    }

    return res;
}

fn print_tiles(tiles: &HashMap<u128, Vec<Vec<char>>>, assembled: &Vec<Vec<u128>>, verbose: bool) {
    if verbose {
        println!("--------------------------------");
        for tile_row in assembled {
            for i in 0..tiles.get(&tile_row[0]).unwrap().len() {
                for tile_id in tile_row {
                    let tile = tiles.get(&tile_id).unwrap();
                    for c in &tile[i] {
                        print!("{}", c);                
                    }
                    print!(" ");
                }
                println!("");
            }
            println!("");
        }
        println!("--------------------------------");
    }
}

fn find_right_neighbor(tiles: &HashMap<u128, Vec<Vec<char>>>, tile_id: u128, edge_index: &HashMap<u128, HashSet<u128>>, verbose: bool) -> Option<(u128, Vec<Vec<char>>)> {
    find_side_neighbor(tiles, tile_id, edge_index, 3, verbose)
}

fn find_bottom_neighbor(tiles: &HashMap<u128, Vec<Vec<char>>>, tile_id: u128, edge_index: &HashMap<u128, HashSet<u128>>, verbose: bool) -> Option<(u128, Vec<Vec<char>>)> {
    find_side_neighbor(tiles, tile_id, edge_index, 1, verbose)
}

fn find_side_neighbor(tiles: &HashMap<u128, Vec<Vec<char>>>, tile_id: u128, edge_index: &HashMap<u128, HashSet<u128>>, side: u128, verbose: bool) -> Option<(u128, Vec<Vec<char>>)> {
    let tile = tiles.get(&tile_id).unwrap();
    let sides = tile_to_edges(&tile);
    let target_side_val = sides[side as usize];
    let matching_tiles = edge_index.get(&target_side_val).unwrap(); //must exist b/c at least current tile_id is in the set.
    if matching_tiles.len() > 2 || matching_tiles.len() == 1 {
        if verbose {
            println!("found no unique neighbor for tile {}, side {}, val {}, neighbors: {:?}", tile_id, side, target_side_val, matching_tiles);
        }
        return None;
    }
    let matching_tile_id = *matching_tiles.iter().filter(|match_tile_id| **match_tile_id != tile_id).nth(0).unwrap();
    let matching_side = match side {
        3 => 2, // needs to be the reversed! because it reads col differently 
        1 => 0,
        _ => {
            panic!("only support matching right and bottom currently")
        }
    };

    let oriented = orient_tile(tiles, matching_tile_id, matching_side, target_side_val, verbose);

    return Some((matching_tile_id, oriented));
}

fn orient_tile(tiles: &HashMap<u128, Vec<Vec<char>>>, tile_id: u128, matching_side: u128, matching_val: u128, verbose: bool) -> Vec<Vec<char>> {
    let tile = tiles.get(&tile_id).unwrap();
    let sides = tile_to_edges(&tile);

    if verbose {
        println!("orienting tile {}, sides: {:?}, to match {} on side {}", tile_id, sides, matching_val, matching_side);
        print_tile(&tile);
    }
    let updated_tile = match (matching_side, sides.iter().position(|&v| v == matching_val)) {
        // match left, found top. flip then rotate left
        (2, Some(0)) => {
            let tmp = flip_left_right(tile);
            rotate(&tmp)
        },

        // match top, found top. do nothing.
        (0, Some(0)) => {
            tile.to_vec()
        },

        // match left, found bottom. rotate right.
        (2, Some(1)) => {
            rotate_right(&tile)
        },
        // match top, found bottom. flip vertical (flip & rotate * 2)
        (0, Some(1)) => {
            let mut tmp = flip_left_right(tile);
            tmp = rotate_right(&tmp);
            rotate_right(&tmp)
        },

        // match left, found left. done.
        (2, Some(2)) => {
            tile.to_vec()
        },
        // match top, found left. rotate right & flip.
        (0, Some(2)) => {
            let tmp = rotate_right(tile);
            flip_left_right(&tmp)
        },

        // match left, found right. flip left-to-right.
        (2, Some(3)) => {
            flip_left_right(tile)
        },
        // match top, found right. rotate left.
        (0, Some(3)) => {
            rotate(&tile)
        },

        // match left, found top (flipped). rotate left.
        (2, Some(4)) => {
            rotate(&tile)
        },
        // match top, found top (flipped). flip l-r. 
        (0, Some(4)) => {
            flip_left_right(tile)
        },

        // match left, found bottom (flipped). flip & rotate right
        (2, Some(5)) => {
            let tmp = flip_left_right(tile);
            rotate_right(&tmp)
        },
        // match top, found bottom (flipped). rotate * 2. 
        (0, Some(5)) => {
            let tmp = rotate(tile);
            rotate(&tmp)
        },

        // match left, found left (flipped)
        (2, Some(6)) => {
            if verbose {
                println!("ok, matched flipped on left, rotating...")
            }
            let mut tmp = rotate(tile);
            if verbose {
                println!("after rotating...");
                print_tile(&tmp);
            }
            tmp = flip_left_right(&tmp);
            if verbose {
                println!("after flipping l-r...");
                print_tile(&tmp);
            }
            let ret = rotate_right(&tmp);
            if verbose {
                println!("after rotating right...");
                print_tile(&tmp);
            }
            ret
        },
        // match top, found left (flipped). rotate right.
        (0, Some(6)) => {
            rotate_right(&tile)
        },

        // match left, found right (flipped). rotate * 2.
        (2, Some(7)) => {
            let tmp = rotate(tile);
            rotate(&tmp)
        },
        // match top, found right (flipped). 
        (0, Some(7)) => {
            let tmp = rotate(tile);
            flip_left_right(&tmp)
        },
        _ => {
            tile.to_vec()
        },
    };

    if verbose {
        let updated_sides = tile_to_edges(&updated_tile);
        println!("done orienting tile {}, sides: {:?}, to match {} on side {}", tile_id, updated_sides, matching_val, matching_side);
        print_tile(&updated_tile);
    }
    return updated_tile.to_vec();
}

fn print_tile(tile: &Vec<Vec<char>>) {
    for row in tile {
        for c in row {
            print!("{}", c);                
        }
        println!("");
    }
}

fn tile_neighbors(tiles: &HashMap<u128, Vec<Vec<char>>>, tile_id: u128, edge_index: &HashMap<u128, HashSet<u128>>) -> [usize; 4] {
    let tile = tiles.get(&tile_id).unwrap();
    let sides = tile_to_edges(&tile);

    let a = [
        edge_index.get(&sides[0]).unwrap().len() - 1, 
        edge_index.get(&sides[1]).unwrap().len() - 1, 
        edge_index.get(&sides[2]).unwrap().len() - 1, 
        edge_index.get(&sides[3]).unwrap().len() - 1, 
    ];
    let b = [
        edge_index.get(&sides[4]).unwrap().len() - 1, 
        edge_index.get(&sides[5]).unwrap().len() - 1, 
        edge_index.get(&sides[6]).unwrap().len() - 1, 
        edge_index.get(&sides[7]).unwrap().len() - 1, 
    ];

    return [
        a[0] + b[0],
        a[1] + b[1],
        a[2] + b[2],
        a[3] + b[3],
    ]
}

fn flip_left_right(tile: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut flipped: Vec<Vec<char>> = Vec::new();
    for line in tile {
        flipped.push(line.iter().map(|c| *c).rev().collect());
    }
    return flipped;
}

fn rotate(tile: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut rotated: Vec<Vec<char>> = Vec::new();
    for i in 0..tile[0].len() {
        rotated.push(col_to_line(tile, (tile[0].len()-1)-i))
    }
    return rotated;
}

fn rotate_right(tile: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut rotated: Vec<Vec<char>> = Vec::new();
    for i in 0..tile[0].len() {
        rotated.push(col_to_line(tile, i).iter().map(|c| *c).rev().collect())
    }
    return rotated;
}

fn day20_part1(fname: &str, verbose: bool) -> usize {
    let mut tiles: HashMap<u128, Vec<Vec<char>>> = HashMap::new();

    let mut curr_tile_id = 0;
    let mut tile_buffer: Vec<Vec<char>> = Vec::new();
    for line in BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok) {
        if line.len() == 0 {
            tiles.insert(curr_tile_id, tile_buffer);
            curr_tile_id = 0;
            tile_buffer = Vec::new();
            continue;
        }
        if curr_tile_id == 0 {
            let parts: Vec<String> = line.split(' ').map(|val| val.to_string()).collect();
            let id_part = parts[1].to_string();
            let end = id_part.len() - 1;
            if verbose {
                println!("split on {} -> {:?}", line, parts);
            }
            curr_tile_id = id_part[..end].parse::<u128>().unwrap();
            continue;
        }
        let chars: Vec<char> = line.chars().collect();
        tile_buffer.push(chars);
    }
    if tile_buffer.len() > 0 {
        tiles.insert(curr_tile_id, tile_buffer);
    }

    if verbose {
        for (k, v) in &tiles {
            println!("{}", k);
            for row in v {
                println!("{:?}", row);
            }
            println!("");
        }
    }

    let mut product = 1;

    let edge_index = build_edge_index(&tiles);
    for (k, _v) in &tiles {
        if count_edges(&tiles, *k, &edge_index, verbose) == 4 {
            product *= k;
        }
    }

    if verbose {
        println!("edge index: {:?}", edge_index);
    }

    return product as usize;
}

fn build_edge_index(tiles: &HashMap<u128, Vec<Vec<char>>>) -> HashMap<u128, HashSet<u128>> {
    let mut index: HashMap<u128, HashSet<u128>> = HashMap::new();

    for (k, v) in tiles.iter() {
        for edge in tile_to_edges(v).iter() {
            index.entry(*edge).or_insert(HashSet::new()).insert(*k);
        }
    }

    return index;
}

fn tile_to_edges(v: &Vec<Vec<char>>) -> [u128; 8] {
    return [
        char_line_to_bitset(&v[0]), 
        char_line_to_bitset(&v[v.len()-1]), 
        char_line_to_bitset(&col_to_line(&v, 0)), 
        char_line_to_bitset(&col_to_line(&v, v.len()-1)),

        char_line_to_bitset(&v[0].iter().rev().map(|c| *c).collect()), 
        char_line_to_bitset(&v[v.len()-1].iter().rev().map(|c| *c).collect()), 
        char_line_to_bitset(&col_to_line(&v, 0).iter().rev().map(|c| *c).collect()), 
        char_line_to_bitset(&col_to_line(&v, v.len()-1).iter().rev().map(|c| *c).collect()),
    ];
}

fn col_to_line(tile: &Vec<Vec<char>>, col: usize) -> Vec<char> {
    let mut ret: Vec<char> = Vec::new();

    for row in tile {
        ret.push(row[col]);
    }

    return ret;
}

fn char_line_to_bitset(line: &Vec<char>) -> u128 {
    let mut bitset: u128 = 0;

    for i in 0..line.len() {
        if line[i] == '#' {
            let mut mask = 1;
            mask = mask << ((line.len()-1) - i);
            bitset |= mask;
        }
    }

    return bitset;
}

fn count_edges(tiles: &HashMap<u128, Vec<Vec<char>>>, tile_id: u128, index: &HashMap<u128, HashSet<u128>>, verbose: bool) -> usize {
    let mut edge_count = 0;

    if let Some(tile) = tiles.get(&tile_id) {
        let sides = tile_to_edges(tile);
        for side in &sides {
            match index.get(side) {
                Some(tile_ids) => {
                    if tile_ids.len() == 1 && tile_ids.contains(&tile_id) {
                        edge_count += 1;
                    }
                }, 
                None => {
                    edge_count += 1;
                }
            }
        }
        if verbose {
            println!("sides for {} are {:?}: [{:#010b},{:#010b},{:#010b},{:#010b},{:#010b},{:#010b},{:#010b},{:#010b}], edge_count: {}", tile_id, sides, sides[0],sides[1],sides[2],sides[3],sides[4],sides[5],sides[6],sides[7], edge_count);
        }
    } else if verbose {
        println!("did not find tile_id {}", tile_id);
    }

    return edge_count;
}

#[derive(Debug, Clone)]
enum MonsterRuleType {
    Refed(Vec<String>),
    Or((Box<MonsterRuleType>, Box<MonsterRuleType>)),
    Value(char),
}

impl MonsterRuleType {
    fn consume(self: &MonsterRuleType, start: usize, val: &Vec<char>, ruleset: &HashMap<String, MonsterRuleType>) -> (bool, Vec<usize>) {
        use MonsterRuleType::*;
        return match self {
            Value(c) => {
                if start >= val.len() {
                    (false, vec![start])
                } else if val[start] == *c {
                    (true, vec![start+1])
                } else {
                    (false, vec![start])
                }
            },
            Or((a, b)) => {
                let ret = a.consume(start, val, ruleset);
                if !ret.0 {
                    b.consume(start, val, ruleset)
                } else {
                    let ret2 = b.consume(start, val, ruleset);
                    if !ret2.0 {
                        ret
                    } else {
                        let mut both: Vec<usize> = Vec::new();
                        both.extend(ret.1);
                        both.extend(ret2.1);
                        (true, both)
                    }
                }
            },
            Refed(refs) => {
                consume_rule(&refs, start, val, ruleset)
            },
        };
    }
}

fn consume_rule(refs: &Vec<String>, start: usize, val: &Vec<char>, ruleset: &HashMap<String, MonsterRuleType>) -> (bool, Vec<usize>) {
    let mut curr_paths: Vec<usize> = Vec::new();
    
    curr_paths.push(start);
    for rule_ref in refs {
        let mut paths: Vec<usize> = Vec::new();
        for curr_path in curr_paths {
            let curr_res = ruleset.get(rule_ref).unwrap().consume(curr_path, val, ruleset);
            if curr_res.0 {
                paths.extend(curr_res.1);
            }
        }
        curr_paths = paths;
    }
    return (curr_paths.len() > 0, curr_paths);
}

fn day19_part1(fname: &str, verbose: bool) -> usize {
    let mut line_iter = BufReader::new(File::open(fname).unwrap()).lines();
    let mut line = line_iter.next().unwrap().unwrap();
    let mut all_rules: HashMap<String, MonsterRuleType> = HashMap::new();

    let mut rule_type: MonsterRuleType;

    while line.len() > 0 {
        let parts: Vec<&str> = line.split(':').collect();
        let key = parts[0];
        let val_parts: Vec<&str> = parts[1].split('|').map(|v| v.trim()).collect();
        if val_parts.len() == 1 {
            if val_parts[0].len() == 3 {
                let vp_chars: Vec<char> = val_parts[0].chars().collect();
                if vp_chars[0] == '"' && vp_chars[2] == '"' {
                    rule_type = MonsterRuleType::Value(vp_chars[1]);
                } else {
                    rule_type = parse_rule_type(val_parts[0].to_string());
                }
            } else {
                rule_type = parse_rule_type(val_parts[0].to_string());
            }
        } else if val_parts.len() == 2 {
            rule_type = MonsterRuleType::Or((Box::new(parse_rule_type(val_parts[0].to_string())), Box::new(parse_rule_type(val_parts[1].to_string()))))
        } else {
            panic!("unexpected pattern");
        }
        all_rules.insert(key.to_string(), rule_type);
        line = line_iter.next().unwrap().unwrap();
    }

    if verbose {
        println!("rules: {:?}", all_rules);
    }

    let rule_0 = all_rules.get("0").unwrap();

    let mut good_count = 0;
    for line in line_iter {
        let line_val = line.unwrap();
        let line_chars = line_val.chars().collect();
        let res = rule_0.consume(0, &line_chars, &all_rules);

        let matched = res.0;
        let terminated = res.1.iter().filter(|offset| **offset == line_val.len()).count() > 0;
        if matched && terminated {
            good_count += 1;
        }
    }

    return good_count;
}

fn parse_rule_type(val: String) -> MonsterRuleType {
    return MonsterRuleType::Refed(val.split(' ').map(|v| v.to_string()).collect());
}

#[derive(Debug)]
enum OpToken {
    Add,
    Multiply,
    ParenOpen,
    ParenClose,
    Value(usize),
}


fn day18_part2(fname: &str, verbose: bool) -> usize {
    return BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok).map(|val| {
        let res = evaluate_expression_v2(&val, verbose);
        if verbose {
            println!("{} = {}", val, res);
        }
        return res;
    }).sum();
}


fn day18_part1(fname: &str, verbose: bool) -> usize {
    return BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok).map(|val| {
        let res = evaluate_expression(&val, verbose);
        if verbose {
            println!("{} = {}", val, res);
        }
        return res;
    }).sum();
}

fn evaluate_expression_v2(expr: &str, _verbose: bool) -> usize {
    let mut tokens: Vec<OpToken> = expr.chars().filter(|c| *c != ' ').map(|c| {
        match c {
            '(' => OpToken::ParenOpen,
            ')' => OpToken::ParenClose,
            '*' => OpToken::Multiply,
            '+' => OpToken::Add,
            _ => OpToken::Value(c.to_digit(10).unwrap() as usize),
        }
    }).collect();

    while tokens.len() > 1 {
        let mut i = 0;
        let mut paren_count = 0;
        while i + 2 < tokens.len() {
            match (&tokens[i], &tokens[i+1], &tokens[i+2]) {
                (OpToken::Value(a), OpToken::Add, OpToken::Value(b)) => {
                    tokens[i] = OpToken::Value(a + b);
                    tokens.remove(i+1);
                    tokens.remove(i+1);
                },
                (OpToken::ParenOpen, _, _) => {
                    paren_count += 1;
                    i += 1;
                },
                _ => {
                    i += 1;
                },
            }
        }
        i = 0;
        while i < tokens.len() {
            if let OpToken::ParenOpen = tokens[i] {
                let mut next_open = 0;
                let mut next_close = 0;
                for j in i+1..tokens.len() {
                    if let OpToken::ParenOpen = tokens[j] {
                        next_open = j;
                    } else if let OpToken::ParenClose = tokens[j] {
                        next_close = j;
                    }
                }
                if next_open == 0 || next_close < next_open {
                    let mut k = i;
                    while k + 3 < tokens.len() {
                        match (&tokens[k], &tokens[k+1], &tokens[k+2], &tokens[k+3]) {
                            (OpToken::ParenOpen, OpToken::Value(a), OpToken::Multiply, OpToken::Value(b)) => {
                                tokens[k+1] = OpToken::Value(a * b);
                                tokens.remove(k+2);
                                tokens.remove(k+2);
                            },
                            _ => {
                                k += 1;
                            },
                        }
                    }
                }
            }
            i += 1;
        }
        i = 0;
        if paren_count == 0 {
            while i + 2 < tokens.len() {
                match (&tokens[i], &tokens[i+1], &tokens[i+2]) {
                    (OpToken::Value(a), OpToken::Multiply, OpToken::Value(b)) => {
                        tokens[i] = OpToken::Value(a * b);
                        tokens.remove(i+1);
                        tokens.remove(i+1);
                    },
                    _ => {
                        i += 1;
                    },
                }
            }
        }
        i = 0;
        while i + 2 < tokens.len() {
            match (&tokens[i], &tokens[i+1], &tokens[i+2]) {
                (OpToken::ParenOpen, OpToken::Value(a), OpToken::ParenClose) => {
                    tokens[i] = OpToken::Value(*a);
                    tokens.remove(i+1);
                    tokens.remove(i+1);
                },
                _ => (),
            }
            i += 1;
        }
    }
    if let OpToken::Value(val) = tokens[0] {
        return val;
    }

    return 0;
}

fn evaluate_expression(expr: &str, _verbose: bool) -> usize {
    let chars: Vec<char> = expr.chars().collect();
    let res = evaluate_expression_from(&chars, 0);
    return res.0;
}

fn evaluate_expression_from(chars: &Vec<char>, index: usize) -> (usize, usize) {
    let mut val = 0;
    let mut curr_index = index;
    let mut multiplying = false;
    while curr_index < chars.len() {
        match chars[curr_index] {
            ' ' => {
                curr_index += 1;
            },
            '+' => {
                curr_index += 1;
            },
            '*' => {
                multiplying = true;
                curr_index += 1;
            },
            '(' => {
                let res = evaluate_expression_from(&chars, curr_index+1);
                curr_index = res.1;
                if multiplying {
                    val *= res.0;
                } else {
                    val += res.0;
                }
                multiplying = false;
            }
            ')' => return (val, curr_index + 1),
            _ => {
                let next_val = chars[curr_index].to_digit(10).unwrap() as usize;
                if multiplying {
                    val *= next_val;
                } else {
                    val += next_val;
                }
                multiplying = false;
                curr_index += 1;
            }
        }

    }
    return (val, chars.len());
}

#[derive(PartialEq, Eq, Hash, Copy, Clone, Debug)]
struct Coord3D {
    x: i32,
    y: i32,
    z: i32,
}

#[derive(PartialEq, Eq, Hash, Copy, Clone, Debug)]
struct Coord4D {
    x: i32,
    y: i32,
    z: i32,
    w: i32,
}

fn day17_part2(fname: &str, _verbose: bool) -> usize {
    let grid: Vec<Vec<char>> = BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok).map(|line| line.chars().collect()).collect();

    let mut active: HashSet<Coord4D> = HashSet::new();
    for x in 0..grid.len() {
        let row = &grid[x];
        for y in 0..row.len() {
            let col = &row[y];
            if *col == '#' {
                active.insert(Coord4D{x:x as i32, y:y as i32, z:0, w:0});
            }
        }
    }

    for _i in 0..6 {
        active = run_cycle_4d(active);
    }

    return active.len();
}

fn day17_part1(fname: &str, _verbose: bool) -> usize {
    let grid: Vec<Vec<char>> = BufReader::new(File::open(fname).unwrap()).lines().filter_map(Result::ok).map(|line| line.chars().collect()).collect();

    let mut active: HashSet<Coord3D> = HashSet::new();
    for x in 0..grid.len() {
        let row = &grid[x];
        for y in 0..row.len() {
            let col = &row[y];
            if *col == '#' {
                active.insert(Coord3D{x:x as i32, y:y as i32, z:0});
            }
        }
    }

    for _i in 0..6 {
        active = run_cycle(active);
    }

    return active.len();
}

fn run_cycle_4d(active: HashSet<Coord4D>) -> HashSet<Coord4D> {
    let mut expanded: HashSet<Coord4D> = HashSet::new();
    for active_cell in &active {
        let neighbors: Vec<Coord4D> = find_neighbors_4d(active_cell);
        for neighbor in neighbors {
            expanded.insert(neighbor);
        }
        expanded.insert(*active_cell);
    }

    let mut updated: HashSet<Coord4D> = HashSet::new();

    for cell in &expanded {
        let neighbors: Vec<Coord4D> = find_neighbors_4d(cell);
        let mut active_count: usize = 0;
        for neighbor in neighbors {
            if active.contains(&neighbor) {
                active_count += 1;
            }        
        }
        if active.contains(cell) {
            if active_count == 2 || active_count == 3 {
                updated.insert(*cell);
            }
        } else {
            if active_count == 3 {
                updated.insert(*cell);
            }
        }
    }
    return updated;
}

fn run_cycle(active: HashSet<Coord3D>) -> HashSet<Coord3D> {
    let mut expanded: HashSet<Coord3D> = HashSet::new();
    for active_cell in &active {
        let neighbors: Vec<Coord3D> = find_neighbors(active_cell);
        for neighbor in neighbors {
            expanded.insert(neighbor);
        }
        expanded.insert(*active_cell);
    }

    let mut updated: HashSet<Coord3D> = HashSet::new();

    for cell in &expanded {
        let neighbors: Vec<Coord3D> = find_neighbors(cell);
        let mut active_count: usize = 0;
        for neighbor in neighbors {
            if active.contains(&neighbor) {
                active_count += 1;
            }        
        }
        if active.contains(cell) {
            if active_count == 2 || active_count == 3 {
                updated.insert(*cell);
            }
        } else {
            if active_count == 3 {
                updated.insert(*cell);
            }
        }
    }
    return updated;
}

fn find_neighbors_4d(cell: &Coord4D) -> Vec<Coord4D> {
    let mut neighbors: Vec<Coord4D> = Vec::new();
    for w in -1..=1 {
        for z in -1..=1 {
            for x in -1..=1 {
                for y in -1..=1 {
                    if x == 0 && y == 0 && z == 0 && w == 0{
                        continue;
                    }
                    neighbors.push(Coord4D{x: cell.x + x, y: cell.y + y, z: cell.z + z, w: cell.w + w});
                }
            }
        }
    }
    return neighbors;
}

fn find_neighbors(cell: &Coord3D) -> Vec<Coord3D> {
    let mut neighbors: Vec<Coord3D> = Vec::new();
    for z in -1..=1 {
        for x in -1..=1 {
            for y in -1..=1 {
                if x == 0 && y == 0 && z == 0 {
                    continue;
                }
                neighbors.push(Coord3D{x: cell.x + x, y: cell.y + y, z: cell.z + z});
            }
        }
    }
    return neighbors;
}

#[derive(Debug)]
struct Range {
    min: usize,
    max: usize,
}

impl Range {
    fn is_valid(self: &Range, val: usize) -> bool {
        return val >= self.min && val <= self.max;
    }
}

#[derive(Debug)]
struct TwoRanges {
    range_one: Range,
    range_two: Range,
}

impl TwoRanges {
    fn is_valid(self: &TwoRanges, val: usize) -> bool {
        return self.range_one.is_valid(val) || self.range_two.is_valid(val);
    }
}

fn day16_part1(fname: &str, verbose: bool) -> usize {
    let rules_re = Regex::new(r"^([^:]+): (\d+)-(\d+) or (\d+)-(\d+)$").unwrap();

    let mut lines_iter = BufReader::new(File::open(fname).unwrap()).lines();

    let mut rules_map: HashMap<String, TwoRanges> = HashMap::new();

    let mut line = lines_iter.next().unwrap().unwrap();
    while line.len() > 0 {
        let matches = rules_re.captures(&line).unwrap();
        if verbose {
            println!("matches for line {}: {:?}", line, matches);
        }
        rules_map.insert(matches[1].to_string(), TwoRanges{
            range_one: Range{
                min: matches[2].parse::<usize>().unwrap(),
                max: matches[3].parse::<usize>().unwrap(),
            },
            range_two: Range{
                min: matches[4].parse::<usize>().unwrap(),
                max: matches[5].parse::<usize>().unwrap(),
            },
        });
        line = lines_iter.next().unwrap().unwrap();
    }
    line = lines_iter.next().unwrap().unwrap();
    while line.len() > 0 {
        line = lines_iter.next().unwrap().unwrap();
        // just skip "my ticket"
    }

    let mut bad_sum = 0;

    lines_iter.next(); // "nearby tickets:"
    line = lines_iter.next().unwrap().unwrap(); // first "nearby ticket"
    while line.len() > 0 {
        let ticket_bad_sum: usize = line.split(',').map(|val| val.parse::<usize>()).filter_map(Result::ok).filter(|val| {
            for (_k, v) in &rules_map {
                if verbose {
                    println!("checking if {} is valid per rule {:?} => {}", val, v, v.is_valid(*val));
                }
                if v.is_valid(*val) {
                    return false; // we're looking for bad, so if we find "good", don't include it.
                }
            }
            if verbose {
                println!("val {} is not valid", val);
            }
            return true;
        })
        .sum();
        if verbose {
            println!("ticket {} had a bad_sum of {}. rules: {:?}", line, ticket_bad_sum, rules_map);
        }
        bad_sum += ticket_bad_sum;
        line = match lines_iter.next() {
            Some(val) => val.unwrap(),
            None => "".to_string(),
        };
    }

    return bad_sum;
}

fn day16_part2(fname: &str, verbose: bool) -> usize {
    let rules_re = Regex::new(r"^([^:]+): (\d+)-(\d+) or (\d+)-(\d+)$").unwrap();

    let mut lines_iter = BufReader::new(File::open(fname).unwrap()).lines();

    let mut rules_map: HashMap<String, TwoRanges> = HashMap::new();

    let mut line = lines_iter.next().unwrap().unwrap();
    while line.len() > 0 {
        let matches = rules_re.captures(&line).unwrap();
        if verbose {
            println!("matches for line {}: {:?}", line, matches);
        }
        rules_map.insert(matches[1].to_string(), TwoRanges{
            range_one: Range{
                min: matches[2].parse::<usize>().unwrap(),
                max: matches[3].parse::<usize>().unwrap(),
            },
            range_two: Range{
                min: matches[4].parse::<usize>().unwrap(),
                max: matches[5].parse::<usize>().unwrap(),
            },
        });
        line = lines_iter.next().unwrap().unwrap();
    }

    lines_iter.next(); // "your ticket"
    let my_ticket: Vec<usize> = lines_iter.next().unwrap().unwrap().split(',').map(|val| val.parse::<usize>()).filter_map(Result::ok).collect(); // my actual ticket

    lines_iter.next(); // blank
    lines_iter.next(); // "nearby tickets:"

    let mut valid_tickets: Vec<Vec<usize>> = lines_iter.map(
        |line| line.unwrap().split(',')
            .map(|val| val.parse::<usize>())
            .filter_map(Result::ok)
            .collect()
    ).filter(
        |ticket| {
            'outer: for &val in ticket {
                for (_k, v) in &rules_map {
                    if v.is_valid(val) {
                       continue 'outer;
                    }
                }
                return false;
            }
            return true;
        }
    ).collect();

    valid_tickets.push(my_ticket.clone());

    if verbose {
        println!("valid_tickets: len={}, {:?}", valid_tickets.len(), valid_tickets);
    }

    let mut field_indices: HashMap<String, Vec<usize>> = HashMap::new();

    for i in 0..valid_tickets.get(0).unwrap().len() {
        let values_at_pos: Vec<usize> = valid_tickets.iter().map(|ticket| ticket[i]).collect();
        'rule_loop: for (k, ruleset) in &rules_map {
            for val in &values_at_pos {
                if !ruleset.is_valid(*val) {
                    continue 'rule_loop;
                }
            }
            field_indices.entry(k.to_string()).or_insert(Vec::new()).push(i);
        }
    }

    if verbose {
        println!("candidates: {:?}", field_indices);
    }

    let mut final_indices: HashMap<String, usize> = HashMap::new();
    let mut done = false;
    while !done {
        let mut to_remove: Vec<(usize, String)> = Vec::new();
        for (k, v) in &field_indices {
            if v.len() == 1 {
                final_indices.insert(k.to_string(), v[0]);
                to_remove.push((v[0], k.to_string()));
            }
        }
        for val in to_remove {
            field_indices.remove(&val.1);
            for (_k, v) in &mut field_indices {
                v.retain(|x| *x != val.0);
            }
        }
        done = field_indices.len() == 0;
    }

    if verbose {
        println!("reduced candidates: {:?}", final_indices);
    }

    let mut running_product = 1;

    for (k, v) in final_indices {
        if k.starts_with("departure") {
            running_product *= my_ticket[v];
        }
    }

    return running_product;
}

fn day15_part2(fname: &str, verbose: bool) -> usize {
    let lines_iter = BufReader::new(File::open(fname).unwrap()).lines();

    let mut last_val = 0;
    for line in lines_iter {
        let line_str = line.unwrap();
        last_val = play_num_game_until(&line_str, 30_000_000, verbose);
        println!("input: {} => {}", line_str, last_val);
    }

    return last_val;
}

fn day15_part1(fname: &str, verbose: bool) -> usize {
    let lines_iter = BufReader::new(File::open(fname).unwrap()).lines();

    let mut last_val = 0;
    for line in lines_iter {
        let line_str = line.unwrap();
        last_val = play_num_game_until(&line_str, 2020, verbose);
        println!("input: {} => {}", line_str, last_val);
    }

    return last_val;
}

fn play_num_game_until(line_str: &str, num_turns: usize, verbose: bool) -> usize {
    let mut seen: HashMap<usize, Vec<usize>> = HashMap::new();

    let mut turn = 0;

    let starting: Vec<usize> = line_str.split(',')
        .map(|val| val.parse::<usize>())
        .filter_map(Result::ok)
        .collect();

    let mut last_spoken: usize = 0;
    for start in starting {
        seen.insert(start, vec!(turn));
        turn += 1;
        last_spoken = start;
    }

    for t in turn..num_turns {
        last_spoken = match seen.get(&last_spoken) {
            Some(turn_seen) if turn_seen.len() > 1 => {
                turn_seen[turn_seen.len()-1] - turn_seen[turn_seen.len()-2]
            },
            _ => 0,
        };
        seen.entry(last_spoken).or_insert_with(|| Vec::new())
            .push(t);
        if verbose {
            println!("the {}th number spoken is {}", t + 1, last_spoken);
        }
    }
    return last_spoken;    
}

fn day14_part2(fname: &str, verbose: bool) -> usize {
    let lines_iter = BufReader::new(File::open(fname).unwrap()).lines();

    let mut registers: HashMap<usize, u64> = HashMap::new();
    let mut curr_mask: Vec<(usize, usize)> = Vec::new();
    for line in lines_iter {
        let line_str = line.unwrap();
        let parts: Vec<&str> = line_str.split(" ").collect();
        if parts[0] == "mask" {
            curr_mask = expand_mask(parts[parts.len()-1].chars().collect(), verbose);
        } else {
            let reg_str = parts[0][4..parts[0].len() - 1].to_string();
            let reg = reg_str.parse::<usize>().unwrap();
            let val = parts[parts.len()-1].parse::<u64>().unwrap();
            for mask in &curr_mask {
                let exp_reg = reg | mask.0;
                let exp_reg = exp_reg & mask.1;
                if verbose {
                    println!("inserting at index. input register: {}, mask_on: {:#010b}, mask_off: {:#010b}, index: {}[{:#010b}]", reg, mask.0, mask.1, exp_reg, exp_reg);
                }
                registers.insert(exp_reg, val);
            }
        }
    }
    if verbose {
        println!("registers: {:?}", registers);
    }
    let mut sum = 0;
    for (_, v) in registers {
        sum += v;
    }
    return sum as usize;
}

fn expand_mask(mask_input: Vec<char>, verbose: bool) -> Vec<(usize, usize)> {
    let mut expanded: Vec<(usize, usize)> = Vec::new();

    let bit_count = mask_input.iter().filter(|&c| *c == 'X').count();
    if verbose {
        println!("expanding for {} X's", bit_count);
    }
    for mask_bits in 0..(2 as usize).pow(bit_count as u32) {
        let mut x_bit = 1;
        let mut mask_on: u64 = 0;
        let mut mask_off: u64 = 0xffff_ffff_ffff_ffff;
        for i in 0..mask_input.len() {
            match mask_input[i] {
                '1' => {
                    let mut m: u64 = 1;
                    m = m << (mask_input.len() - (i+1));
                    mask_on |= m;
                },
                'X' => {
                    let mut m: u64 = 1;
                    m = m << (bit_count - x_bit);
                    if (m & (mask_bits as u64)) != 0 {
                        m = 1;
                        m = m << (mask_input.len() - (i+1));
                        mask_on |= m;
                    } else {
                        m = 1;
                        m = m << (mask_input.len() - (i+1));
                        mask_off = !mask_off;
                        mask_off |= m;
                        mask_off = !mask_off;
                    }
                    x_bit += 1;
                },
                _ => (),
            }
        }
        expanded.push((mask_on as usize, mask_off as usize));  
    }
    if verbose {
        println!("expanded len {}", expanded.len());
    }
    return expanded;
}

fn day14_part1(fname: &str, verbose: bool) -> usize {
    let lines_iter = BufReader::new(File::open(fname).unwrap()).lines();

    let mut masks = (0, 0);
    let mut registers: HashMap<usize, u64> = HashMap::new();
    for line in lines_iter {
        let line_str = line.unwrap();
        let parts: Vec<&str> = line_str.split(" ").collect();
        if parts[0] == "mask" {
            masks = parse_reg_mask(&parts[parts.len()-1].chars().collect());
        } else {
            let reg_str = parts[0][4..parts[0].len() - 1].to_string();
            let reg = reg_str.parse::<usize>().unwrap();
            let mut val = parts[parts.len()-1].parse::<u64>().unwrap();
            if verbose {
                println!("val: {}\nval_b: {:#040b}\nmask_on: #{:#040b}\nmask_off: {:#040b}\nval|mask_on: #{:#040b}\nval&mask_off: {:#040b}", val, val, masks.0, masks.1, val | masks.0, val & masks.1);
            }
            val |= masks.0;
            val &= masks.1;
            if verbose {
                println!("val (after): {:#040b}", val);
            }
            registers.insert(reg, val);
        }
    }
    if verbose {
        println!("registers: {:?}", registers);
    }
    let mut sum = 0;
    for (_, v) in registers {
        sum += v;
    }
    return sum as usize;
}

fn parse_reg_mask(mask_input: &Vec<char>) -> (u64, u64) {
    let mut mask_on: u64 = 0;
    let mut mask_off: u64 = 0xffff_ffff_ffff_ffff;

    for i in 0..mask_input.len() {
        match mask_input[i] {
            '1' => {
                let mut m: u64 = 1;
                m = m << (mask_input.len() - (i+1));
                mask_on |= m;
            },
            '0' => {
                let mut m: u64 = 1;
                m = m << (mask_input.len() - (i+1));
                mask_off = !mask_off;
                mask_off |= m;
                mask_off = !mask_off;
            },
            _ => (),
        }
    }
    return (mask_on, mask_off);    
}

fn day13_part2(fname: &str, _verbose: bool) -> usize {
    let mut lines_iter = BufReader::new(File::open(fname).unwrap()).lines();
    let _estimate = lines_iter.next().unwrap().unwrap().parse::<u64>().unwrap();
    let bus_ids: Vec<u64> = lines_iter.next().unwrap().unwrap()
        .split(',')
        .map(|val| {
            match val.parse::<u64>() {
                Result::Ok(val) => val,
                _ => 0,
            }
        }).collect();

    // i didn't have the math skills to get this one. My solution worked, but was way too slow 
    // (sample was fine, real data taking forever). Got this from @lizthegrey's livestream. Thanks Liz!
    let mut running_product: u64 = 1;
    let mut min_value: u64 = 0;
    for i in 0..bus_ids.len() {
        let bus_id = bus_ids[i];
        if bus_id == 0 {
            continue;
        }
        while (min_value + i as u64) % bus_id != 0 {
            min_value += running_product;
        }
        running_product *= bus_id;
    }
    println!("value as u64: {}", min_value);
    return min_value as usize;
}


fn day13_part1(fname: &str, verbose: bool) -> usize {
    let mut lines_iter = BufReader::new(File::open(fname).unwrap()).lines();
    let estimate = lines_iter.next().unwrap().unwrap().parse::<usize>().unwrap();
    let bus_ids: Vec<usize> = lines_iter.next().unwrap().unwrap()
        .split(',')
        .map(|val| val.parse::<usize>())
        .filter_map(Result::ok)
        .collect();

    let mut min_wait: (usize, usize) = (0,0);
    for bus_id in bus_ids {
        let wait_time = bus_id - (estimate % bus_id);
        if min_wait.0 == 0 || wait_time < min_wait.1 {
            min_wait = (bus_id, wait_time);
        }
    }
    if verbose {
        println!("min_wait: {:?}, estimate: {}, mod: {}", min_wait, estimate, estimate % min_wait.0);
    }

    return min_wait.0 * min_wait.1;
}

fn day12_part2(fname: &str, verbose: bool) -> usize {
    let mut pos: (isize, isize) = (10, 1);
    let mut ship: (isize, isize) = (0, 0);
    for line_res in BufReader::new(File::open(fname).unwrap()).lines() {
        let line = line_res.unwrap();
        let action = &line[0..1];
        let distance = line[1..].parse::<isize>().unwrap();

        match action {
            "N" => {
                pos.1 += distance;
            },
            "S" => {
                pos.1 -= distance;
            },
            "E" => {
                pos.0 += distance;
            },
            "W" => {
                pos.0 -= distance;
            },
            "L" => {
                for _i in 0..(distance/90) {
                    pos = (-pos.1, pos.0);
                }
            }, 
            "R" => {
                for _i in 0..(distance/90) {
                    pos = (pos.1, -pos.0);
                }
            },
            "F" => {
                ship.0 += pos.0 * distance;
                ship.1 += pos.1 * distance;
            },
            _ => {
                panic!("unrecognized action");
            }
        }
        if verbose {
            println!("pos after {}: ({},{})", line, pos.0, pos.1);
        }
    }

    if verbose {
        println!("final pos: ({},{})", pos.0, pos.1);
    }

    return (ship.0.abs() + ship.1.abs()) as usize;
}

fn day12_part1(fname: &str, verbose: bool) -> usize {
    let mut dir: isize = 0;
    let mut pos: (isize, isize) = (0, 0);
    for line_res in BufReader::new(File::open(fname).unwrap()).lines() {
        let line = line_res.unwrap();
        let action = &line[0..1];
        let distance = line[1..].parse::<isize>().unwrap();

        match action {
            "N" => {
                pos.1 += distance;
            },
            "S" => {
                pos.1 -= distance;
            },
            "E" => {
                pos.0 += distance;
            },
            "W" => {
                pos.0 -= distance;
            },
            "L" => {
                dir += distance;
                dir = dir % 360;
            }, 
            "R" => {
                dir -= distance;
                if dir < 0 {
                    dir = 360 + dir;
                }
                dir = dir % 360;
            },
            "F" => {
                match dir {
                    0 => {
                        pos.0 += distance;
                    },
                    90 => {
                        pos.1 += distance;
                    },
                    180 => {
                        pos.0 -= distance;
                    },
                    270 => {
                        pos.1 -= distance;
                    },
                    _ => {
                        panic!("Unsupported heading {}", dir);
                    }
                }
            },
            _ => {
                panic!("unrecognized action");
            }
        }
        if verbose {
            println!("pos after {}: ({},{})", line, pos.0, pos.1);
        }
    }

    if verbose {
        println!("final pos: ({},{})", pos.0, pos.1);
    }

    return (pos.0.abs() + pos.1.abs()) as usize;
}

fn day11_part2(fname: &str, verbose: bool) -> usize {
    let mut board: Vec<Vec<char>> = BufReader::new(File::open(fname).unwrap())
        .lines()
        .filter_map(Result::ok)
        .map(|line| line.chars().collect())
        .collect();
    
    let mut occupied = 0;
    let mut done = false;
    while !done {
        let (next, next_occupied) = tick_seats_2(board);
        done = next_occupied == occupied;
        occupied = next_occupied;
        board = next;
    }
    if verbose {
        print_board(&board);
    }
    return occupied;
}

fn tick_seats_2(board: Vec<Vec<char>>) -> (Vec<Vec<char>>, usize) {
    let mut next: Vec<Vec<char>> = Vec::new();
    let mut occupied = 0;
    for i in 0..board.len() {
        let mut next_row: Vec<char> = Vec::new();

        let row = board.get(i).unwrap();
        for j in 0..row.len() {
            let seat = determine_next_seat_state_2(&board, (i, j));
            if seat == '#' {
                occupied += 1;
            }
            next_row.push(seat);
        }
        next.push(next_row);
    }
    return (next, occupied);
}

fn determine_next_seat_state_2(board: &Vec<Vec<char>>, coord: (usize, usize)) -> char {
    match board.get(coord.0).unwrap().get(coord.1).unwrap() {
        '.' => '.',
        'L' => {
            if count_visible_neighbors(&board, coord) == 0 {
                '#'
            } else {
                'L'
            }
        },
        '#' => {
            if count_visible_neighbors(&board, coord) >= 5 {
                'L'
            } else {
                '#'
            }
        },
        _ => {
            panic!("unsupported board char");
        },
    }   
}

fn count_visible_neighbors(board: &Vec<Vec<char>>, coord: (usize, usize)) -> usize {
    let mut count = 0;
    for i in -1..2 {
        for j in -1..2 {
            if i == 0 && j == 0 {
                continue;
            }

            // have a vector now. follow vector until we hit a seat or off board. 
            if line_of_sight(coord, (i, j), &board) {
                count += 1;
            }
        }
    }
    return count;
}

fn line_of_sight(start: (usize, usize), vector: (isize, isize), board: &Vec<Vec<char>>) -> bool {
    let mut curr = (start.0 as isize + vector.0, start.1 as isize + vector.1);

    while in_bounds(curr, board) {
        match board.get(curr.0 as usize).unwrap().get(curr.1 as usize).unwrap() {
            'L' => {
                return false;
            },
            '#' => {
                return true;
            },
            _ => (),
        }
        curr = (curr.0 + vector.0, curr.1 + vector.1);
    }
    return false;
}

fn in_bounds(coord: (isize, isize), board: &Vec<Vec<char>>) -> bool {
    if coord.0 < 0 || coord.0 > board.len() as isize - 1 {
        return false;
    }
    let row = board.get(coord.0 as usize).unwrap();
    if coord.1 < 0 || coord.1 > row.len() as isize - 1 {
        return false;
    }
    return true;
}

fn day11_part1(fname: &str, verbose: bool) -> usize {
    let mut board: Vec<Vec<char>> = BufReader::new(File::open(fname).unwrap())
        .lines()
        .filter_map(Result::ok)
        .map(|line| line.chars().collect())
        .collect();
    
    let mut occupied = 0;
    let mut done = false;
    while !done {
        let (next, next_occupied) = tick_seats(board);
        done = next_occupied == occupied;
        occupied = next_occupied;
        board = next;
    }
    if verbose {
        print_board(&board);
    }
    return occupied;
}

fn print_board(board: &Vec<Vec<char>>) {
    for row in board {
        for spot in row {
            print!("{}", spot);
        }
        print!("\n");
    }
    println!("-------------");
}

fn tick_seats(board: Vec<Vec<char>>) -> (Vec<Vec<char>>, usize) {
    let mut next: Vec<Vec<char>> = Vec::new();
    let mut occupied = 0;
    for i in 0..board.len() {
        let mut next_row: Vec<char> = Vec::new();

        let row = board.get(i).unwrap();
        for j in 0..row.len() {
            let seat = determine_next_seat_state(&board, (i, j));
            if seat == '#' {
                occupied += 1;
            }
            next_row.push(seat);
        }
        next.push(next_row);
    }
    return (next, occupied);
}

fn determine_next_seat_state(board: &Vec<Vec<char>>, coord: (usize, usize)) -> char {
    match board.get(coord.0).unwrap().get(coord.1).unwrap() {
        '.' => '.',
        'L' => {
            if count_neighbors(&board, coord) == 0 {
                '#'
            } else {
                'L'
            }
        },
        '#' => {
            if count_neighbors(&board, coord) >= 4 {
                'L'
            } else {
                '#'
            }
        },
        _ => {
            panic!("unsupported board char");
        },
    }   
}

fn count_neighbors(board: &Vec<Vec<char>>, coord: (usize, usize)) -> usize {
    let mut count = 0;
    for i in -1..2 {
        for j in -1..2 {
            if i == 0 && j == 0 {
                continue;
            }

            let x = coord.0 as isize + i;
            if x < 0 || x > board.len() as isize - 1 {
                continue;
            }

            let row = board.get(x as usize).unwrap();
            let y = coord.1 as isize + j;
            if y < 0 || y > row.len() as isize - 1 {
                continue;
            }

            if *row.get(y as usize).unwrap() == '#' {
                count += 1;
            }
        }
    }
    return count;
}

fn day10_part2(fname: &str, verbose: bool) -> usize {
    let mut values: Vec<usize> = BufReader::new(File::open(fname).unwrap())
        .lines()
        .filter_map(Result::ok)
        .map(|line| line.parse::<usize>())
        .filter_map(Result::ok)
        .collect();

    values.sort();

    return count_compatible_adapters(0, &values, verbose, &mut HashMap::new());
}

fn count_compatible_adapters(joltage: usize, values: &Vec<usize>, verbose: bool, cached: &mut HashMap<usize, usize>) -> usize {
    let mut count: usize = 0;

    let children = find_compatible_adapters(joltage, &values);
    if verbose {
        println!("found {} children for joltage {}", children.len(), joltage);
    }
    if children.len() == 0 {
        return 1;
    }
    for child in children {
        if let None = cached.get(&child) {
            let cca = count_compatible_adapters(child, &values, verbose, cached);
            cached.insert(child, cca);
        }
        count += cached.get(&child).unwrap();
    }
    return count as usize;
}

fn find_compatible_adapters(joltage: usize, values: &Vec<usize>) -> Vec<usize> {
    let mut found: Vec<usize> = Vec::new();
    for val in values {
        if *val <= joltage {
            continue;
        }
        if val - joltage <= 3 {
            found.push(*val);
        } else { 
            return found;
        }
    }
    return found;
}
/*
         0
        / \
        1  2
       / |  | 
      2  3  3
        */

fn day10_part1(fname: &str, verbose: bool) -> usize {
    let mut values: Vec<usize> = BufReader::new(File::open(fname).unwrap())
        .lines()
        .filter_map(Result::ok)
        .map(|line| line.parse::<usize>())
        .filter_map(Result::ok)
        .collect();

    values.sort();

    let mut one_diffs = 0;
    let mut three_diffs = 1;
    let mut last = 0;
    for val in values {
        let diff = val - last;
        match diff {
            1 => one_diffs += 1,
            3 => three_diffs += 1,
            _ => {},
        }
        last = val;
    }

    if verbose {
        println!("one_diffs: {}, three_diffs: {}", one_diffs, three_diffs);
    }

    return one_diffs * three_diffs;
}

fn day9_part2(fname: &str, verbose: bool, lookback: i32) -> usize {
    let target = day9_part1(fname, verbose, lookback) as i64;

    let values: Vec<i64> = BufReader::new(File::open(fname).unwrap())
        .lines()
        .filter_map(Result::ok)
        .map(|line| line.parse::<i64>())
        .filter_map(Result::ok)
        .collect();

    let mut low = 0;
    let mut high;

    if verbose {
        println!("looking for a contiguous range that sums to {}", target);
    }

    for i in 2..values.len() {
        let mut sum = calc_sum(&values[low..i]);
        if verbose {
            println!("checking sum from {} to {} = {}", low, i, sum);
        }
        if sum == target {
            return min_max_sum(&values[low..i]) as usize;
        }
        if sum > target {
            low += 1;
            high = low+1;
            while high != i + 1 {
                high += 1;
                sum = calc_sum(&values[low..high]);
                if sum == target {
                    return min_max_sum(&values[low..high]) as usize;
                }
            }
        }
    }
    panic!("no solution found!");
}

fn min_max_sum(values: &[i64]) -> i64 {
    let mut min = 0; 
    let mut max = 0;
    for i in 0..values.len() {
        if i == 0 {
            min = values[i];
            max = values[i];
            continue;
        }
        if values[i] < min {
            min = values[i];
        }
        if values[i] > max {
            max = values[i];
        }
    }
    return min + max;
}

fn calc_sum(values: &[i64]) -> i64 {
    return values.iter().fold(0, |a, b| a + b);
}

fn day9_part1(fname: &str, verbose: bool, lookback: i32) -> usize {
    let mut buffer: Vec<i32> = vec![0; lookback as usize];
    let mut count: i32 = 0;

    for line_res in BufReader::new(File::open(fname).unwrap()).lines() {
        let number = line_res.unwrap().parse::<i32>().unwrap();
        let index = count % lookback;
        count += 1;
        if count > lookback {
            if verbose {
                println!("checking for a sum to {}, in {:?}", number, buffer);
            }
            if let None = find_sum_pair(&buffer, number) {
                return number as usize;
            }
        }
        buffer[index as usize] = number;
    }

    return 0;
}

fn find_sum_pair(values: &Vec<i32>, sum: i32) -> Option<(i32, i32)> {
    for i in 0..values.len() {
        for j in 0..values.len() {
            if values[i] + values[j] == sum {
                return Some((values[i], values[j]));
            }
        }
    }
    return None;
}

fn day8_part2(fname: &str, verbose: bool) -> usize {
    let inst_re = Regex::new(r"^(acc|jmp|nop) (\+|-)(\d+)$").unwrap();

    let reader = BufReader::new(File::open(fname).unwrap());
    let mut stack: Vec<Instruction> = reader.lines()
        .filter_map(Result::ok)
        .map(|line| {
            let caps = inst_re.captures(&line).unwrap();
            if verbose {
                println!("matches for line {}, caps: {:?}", line, caps);
            }
            let mut val = caps[3].parse::<i32>().unwrap();
            if caps[2] == *"-" {
                val *= -1;
            }
            return match &caps[1] {
                "acc" => Instruction::Acc(val),
                "jmp" => Instruction::Jmp(val),
                _ => Instruction::Nop(val),
            }
        })
        .collect();
    
    for i in 0..stack.len() {
        let swap: Option<(Instruction, Instruction)>;
        {
            let instruction = stack.get(i).unwrap();
            swap = match instruction {
                Instruction::Jmp(val) => {
                    Some((Instruction::Jmp(*val), Instruction::Nop(*val)))
                },
                Instruction::Nop(val) => {
                    Some((Instruction::Nop(*val), Instruction::Jmp(*val)))
                },
                _ => {
                    None
                },
            };
        }
        if let Some(swap) = swap {
            stack[i] = swap.1;
            match run_program(&stack) {
                Some(answer) => {
                    if answer < 0 {
                        panic!("answer is negative {}", answer);
                    }
                    return answer as usize;
                },
                None => {
                }
            }
            stack[i] = swap.0;
        }
    }
    panic!("no solution found.");
}

fn run_program(stack: &Vec<Instruction>) -> Option<i32> {
    let mut seen = HashSet::new();
    let mut state: (i32, i32) = (0, 0);
    while !seen.contains(&state.0) && state.0 != stack.len() as i32 {
        let index = state.0;
        seen.insert(index);
        state = match stack.get(index as usize) {
            Some(inst) => {
                inst.execute(state)
            },
            None => {
                panic!("attempt to run instruction out of stack {}", index);
            }
        };
    }
    if state.1 < 0 {
        panic!("value is negative! {}", state.1);
    }

    if state.0 == stack.len() as i32 {
        return Some(state.1);
    }
    return None;
}

fn day8_part1(fname: &str, verbose: bool) -> usize {
    let inst_re = Regex::new(r"^(acc|jmp|nop) (\+|-)(\d+)$").unwrap();

    let reader = BufReader::new(File::open(fname).unwrap());
    let lines: Vec<Instruction> = reader.lines()
        .filter_map(Result::ok)
        .map(|line| {
            let caps = inst_re.captures(&line).unwrap();
            if verbose {
                println!("matches for line {}, caps: {:?}", line, caps);
            }
            let mut val = caps[3].parse::<i32>().unwrap();
            if caps[2] == *"-" {
                val *= -1;
            }
            return match &caps[1] {
                "acc" => Instruction::Acc(val),
                "jmp" => Instruction::Jmp(val),
                _ => Instruction::Nop(val),
            }
        })
        .collect();
    
    let mut seen = HashSet::new();
    let mut state: (i32, i32) = (0, 0);
    while !seen.contains(&state.0) {
        let index = state.0;
        seen.insert(index);
        state = match lines.get(index as usize) {
            Some(inst) => {
                inst.execute(state)
            },
            None => {
                panic!("attempt to run instruction out of stack {}", index);
            }
        };
    }
    if state.1 < 0 {
        println!("value is negative!");
    }
    return state.1 as usize;
}

enum Instruction {
    Acc(i32),
    Jmp(i32),
    Nop(i32),
}

impl Instruction {
    fn execute(&self, curr_state: (i32, i32)) -> (i32, i32) {
        return match *self {
            Instruction::Acc(increment) => (curr_state.0 + 1, curr_state.1 + increment),
            Instruction::Jmp(offset) => (curr_state.0 + offset, curr_state.1),
            _ => (curr_state.0 + 1, curr_state.1),
        }
    }
}

fn day7_part2(fname: &str, verbose: bool) -> usize {
    let mut bags: HashMap<String, HashMap<String, usize>> = HashMap::new();

    let bag_rule_re = Regex::new(r"(\d+) ([^,.]+) bag[s]?[,.]").unwrap();

    let reader = BufReader::new(File::open(fname).unwrap());
    for line_res in reader.lines() {
        let line = line_res.unwrap();
        let line_parts: Vec<&str> = line.split(" bags contain ").collect();
        let bag = line_parts.get(0).unwrap().to_string();
        let contents = line_parts.get(1).unwrap();

        if bags.contains_key(&bag) {
            panic!("found multiple rules for the same bag: {}", bag);
        }
        let mut inner_bags: HashMap<String, usize> = HashMap::new();
        for cap in bag_rule_re.captures_iter(contents) {
            inner_bags.insert(cap[2].to_string(), cap[1].parse::<usize>().unwrap());
        }
        bags.insert(bag, inner_bags);
    }

    if verbose {
        println!("bags: {:?}", bags);
    }

    return count_children("shiny gold", &bags, verbose);
}

fn count_children(bag: &str, all_rules: &HashMap<String, HashMap<String, usize>>, verbose: bool) -> usize {
    if verbose {
        println!("count_children for {} bag.", bag);
    }
    match all_rules.get(&bag.to_string()) {
        Some(bag_rules) => {
            let mut count = 0;
            for (key, value) in bag_rules {
                count += value + (value * count_children(key, all_rules, verbose));
            }
            if verbose {
                println!("done with count_children for {} bag. got {}", bag, count);
            }
            return count;
        }, 
        None => {
            if verbose {
                println!("{} bag had no children.", bag);
            }
            return 0;
        }
    }
}

fn day7_part1(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    let lines: Vec<String> = reader.lines().filter_map(Result::ok).collect();
    let mut has_path: HashSet<&str> = HashSet::new();

    let mut found: HashSet<&str> = HashSet::new();
    found.insert("shiny gold");
    while found.len() > 0 {
        has_path.extend(&found);
        found.clear();
        
        for line in &lines {
            let line_parts: Vec<&str> = line.split(" bags contain ").collect();
            let bag = line_parts.get(0).unwrap();
            let contents = line_parts.get(1).unwrap();

            let mut was_found = "no";
            for check in &has_path {
                if verbose {
                    println!("checking: {}, found?: {:?}, exists?: {}", check, contents.find(check), has_path.contains(check));
                }
                if has_path.contains(bag) {
                    continue;
                }
                if let Some(_index) = contents.find(check) {
                    found.insert(bag);
                    was_found = bag;
                }
            }
            if verbose {
                println!("line: {}, bag: {}, contents: {}, was_found: {}", line, bag, contents, was_found);
            }
        }
    }

    return has_path.len() - 1;
}

fn day6_part2(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    let mut marks_sum = 0;
    let mut group_marks: u32 = 0xffffffff;
    let mut marks: u32 = 0;
    for line_res in reader.lines() {
        let line = line_res.unwrap();
        if verbose {
            println!("line: {}", line);
        }
        if line.len() == 0 {
            marks_sum += group_marks.count_ones();
            if verbose {
                println!("marks: {:#032b}, ones: {}\n----------------------", group_marks, group_marks.count_ones());
            }
            group_marks = 0xffffffff;
            continue;
        }
        for q in line.bytes() {
            let offset = q - b'a';
            let mut mask: u32 = 1;
            mask = mask << offset;
            marks = marks | mask;
        }
        group_marks = group_marks & marks;
        marks = 0;
    }
    marks_sum += group_marks.count_ones();
    return marks_sum as usize;
}

fn day6_part1(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    let mut marks_sum = 0;
    let mut marks: u32 = 0;
    for line_res in reader.lines() {
        let line = line_res.unwrap();
        if verbose {
            println!("line: {}", line);
        }
        if line.len() == 0 {
            marks_sum += marks.count_ones();
            if verbose {
                println!("marks: {:#b}, ones: {}\n----------------------", marks, marks.count_ones());
            }
            marks = 0;
        }
        for q in line.bytes() {
            let offset = q - b'a';
            let mut mask: u32 = 1;
            mask = mask << offset;
            marks = marks | mask;
        }
    }
    marks_sum += marks.count_ones();
    return marks_sum as usize;
}

fn day5_part2(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    let mut occupied: Vec<usize> = reader.lines().filter_map(Result::ok).map(|ticket| {
        let mut lower = 0;
        let mut upper = 127;
        let mut letters_iter = ticket.chars();
        for _r in 1..8 {
            let diff = (upper - lower) + 1;
            let letter = letters_iter.next().unwrap();
            match letter {
                'F' => {
                    if diff == 1 {
                        lower = upper;
                    } else {
                        upper = upper - diff/2;
                    }
                },
                'B' => {
                    if diff == 1 {
                        upper = lower;
                    } else {
                        lower = lower + diff/2;
                    }
                },
                _ => {
                    if verbose {
                        println!("got a bad word. {}", letter);
                    }
                    panic!("bad.");
                }
            }
        }
        let row = lower;
        lower = 0;
        upper = 7;
        for _c in 1..4 {
            let diff = (upper - lower) + 1;
            let letter = letters_iter.next().unwrap();
            match letter {
                'L' => {
                    if diff == 1 {
                        lower = upper;
                    } else {
                        upper = upper - diff/2;
                    }
                },
                'R' => {
                    if diff == 1 {
                        upper = lower;
                    } else {
                        lower = lower + diff/2;
                    }
                },
                _ => {
                    if verbose {
                        println!("got a bad word. {}", letter);
                    }
                    panic!("bad.");
                }
            }
        }
        let col = lower;
        if verbose {
            println!("{}: row {}, column {}, seat ID: {}.", ticket, row, col, row * 8 + col)
        }
        return row * 8 + col;
    }).collect();
    occupied.sort();
    let mut occupied_iter = occupied.iter();
    let mut prev = *occupied_iter.next().unwrap();
    for val in occupied_iter {
        let curr = *val;
        if verbose {
            println!("prev/curr: {}/{}", prev, curr);
        }
        if curr != prev + 1 {
            return prev + 1;
        }
        prev = curr;
    }
    return 0;
}

fn day5_part1(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    return reader.lines().filter_map(Result::ok).map(|ticket| {
        let mut lower = 0;
        let mut upper = 127;
        let mut letters_iter = ticket.chars();
        for _r in 1..8 {
            let diff = (upper - lower) + 1;
            let letter = letters_iter.next().unwrap();
            match letter {
                'F' => {
                    if diff == 1 {
                        lower = upper;
                    } else {
                        upper = upper - diff/2;
                    }
                },
                'B' => {
                    if diff == 1 {
                        upper = lower;
                    } else {
                        lower = lower + diff/2;
                    }
                },
                _ => {
                    if verbose {
                        println!("got a bad word. {}", letter);
                    }
                    panic!("bad.");
                }
            }
        }
        let row = lower;
        lower = 0;
        upper = 7;
        for _c in 1..4 {
            let diff = (upper - lower) + 1;
            let letter = letters_iter.next().unwrap();
            match letter {
                'L' => {
                    if diff == 1 {
                        lower = upper;
                    } else {
                        upper = upper - diff/2;
                    }
                },
                'R' => {
                    if diff == 1 {
                        upper = lower;
                    } else {
                        lower = lower + diff/2;
                    }
                },
                _ => {
                    if verbose {
                        println!("got a bad word. {}", letter);
                    }
                    panic!("bad.");
                }
            }
        }
        let col = lower;
        if verbose {
            println!("{}: row {}, column {}, seat ID: {}.", ticket, row, col, row * 8 + col)
        }
        return row * 8 + col;
    }).max().unwrap();
}

fn day4_part2(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    let mut good_count = 0;
    let mut passport: u8 = 0;
    let mut invalid = false;
    for line_res in reader.lines() {
        let line = line_res.unwrap();
        if line.len() == 0 {
            if !invalid && check_passport(passport) {
                good_count += 1;
            }
            passport = 0;
            invalid = false;
            continue;
        }
        for pair in line.split(' ') {
            if verbose {
                println!("pair: {}", pair);
            }
            let mut pair_iter = pair.split(':');
            let key = pair_iter.next().unwrap();
            let val = pair_iter.next().unwrap();
            if verbose {
                println!("pair: {}, key: {}, val: {}", pair, key, val);
            }
            let mut mask: u8 = 0;
            match key {
                "byr" => {
                    mask = 0b0000_0001;
                    if let Ok(year) = val.parse::<usize>() {
                        if year < 1920 || year > 2002 {
                            invalid = true;
                        }
                    } else {
                        invalid = true;
                    }
                },
                "iyr" => {
                    mask = 0b0000_0010;
                    if let Ok(year) = val.parse::<usize>() {
                        if year < 2010 || year > 2020 {
                            invalid = true;
                        }
                    } else {
                        invalid = true;
                    }
                },
                "eyr" => {
                    mask = 0b0000_0100;
                    if let Ok(year) = val.parse::<usize>() {
                        if year < 2020 || year > 2030 {
                            invalid = true;
                        }
                    } else {
                        invalid = true;
                    }
                }
                "hgt" => {
                    mask = 0b0000_1000;
                    if val.len() < 3 {
                        invalid = true;
                    } else {
                        let unit = &val[val.len()-2..];
                        let measure = val[..val.len()-2].parse::<usize>().unwrap();
                        if verbose {
                            println!("hgt, pair: {}, unit: {}, measure: {}", pair, unit, measure);
                        }
                        let (min, max) = match unit {
                            "cm" => (150, 193),
                            "in" => (59, 76),
                            _ => (0, 0),
                        };
                        if measure < min || measure > max {
                            invalid = true;
                        }
                    }
                },
                "hcl" => {
                    mask = 0b0001_0000;
                    if val.len() != 7 {
                        invalid = true;
                    } else {
                        let mut char_iter = val.chars();
                        if char_iter.next().unwrap() != '#' {
                            invalid = true;
                            continue;
                        }
                        for _x in 1..6 {
                            let letter = char_iter.next().unwrap();
                            if (letter >= '0' && letter <= '9') || (letter >= 'a' && letter <= 'f') {
                                // all good.
                            } else {
                                invalid = true;
                            }
                        }
                    }
                },
                "ecl" => {
                    mask = 0b0010_0000;
                    let mut unknown = false;
                    match val {
                        "amb" => (),
                        "blu" => (),
                        "brn" => (),
                        "gry" => (),
                        "grn" => (),
                        "hzl" => (),
                        "oth" => (),
                        _ => {
                            unknown = true;
                        }
                    }
                    if unknown {
                        invalid = true;
                    }
                },
                "pid" => {
                    mask = 0b0100_0000;
                    if val.len() != 9 {
                        invalid = true;
                    } else {
                        let mut char_iter = val.chars();
                        for _x in 1..9 {
                            let letter = char_iter.next().unwrap();
                            if letter >= '0' && letter <= '9' {
                                // all good.
                            } else {
                                invalid = true;
                            }
                        }
                    }
                },
                "cid" => {
                    mask = 0b1000_0000;
                },
                _ => (),
            };
            passport |= mask;
        }
        if verbose {
            println!("line: {}, passport: {:#010b}", line, passport);
        }
    }
    if !invalid && passport > 0 && check_passport(passport) {
        good_count += 1;
    }
    return good_count;
}

fn day4_part1(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    let mut good_count = 0;
    let mut passport: u8 = 0;
    for line_res in reader.lines() {
        let line = line_res.unwrap();
        if line.len() == 0 {
            if check_passport(passport) {
                good_count += 1;
            }
            passport = 0;
        }
        for pair in line.split(' ') {
            let key = pair.split(':').next().unwrap();
            let mask = match key {
                "byr" => 0b0000_0001,
                "iyr" => 0b0000_0010,
                "eyr" => 0b0000_0100,
                "hgt" => 0b0000_1000,
                "hcl" => 0b0001_0000,
                "ecl" => 0b0010_0000,
                "pid" => 0b0100_0000,
                "cid" => 0b1000_0000,
                _ => 0,
            };
            passport |= mask;
        }
        if verbose {
            println!("line: {}, passport: {:#010b}", line, passport);
        }
    }
    if passport > 0 && check_passport(passport) {
        good_count += 1;
    }
    return good_count;
}

const ALL: u8 = 0b0111_1111;

fn check_passport(passport: u8) -> bool {
    return (passport & ALL) == ALL;
}

fn day3_part2(fname: &str, verbose: bool) -> usize {
    let slopes = [(1,1),(3,1),(5,1),(7,1),(1,2)];

    let answers: Vec<usize> = slopes.iter().map(|pair| day3_count_trees(pair.0, pair.1, fname, verbose)).collect();

    if verbose {
        println!("got answers {:?}", answers)
    }
    let mut answer = 1;
    for val in answers {
        answer = answer * val;
    }

    return answer;
}

fn day3_count_trees(right: usize, down: usize, fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    
    let mut current_x = 0;
    let mut tree_hit = 0;

    let mut iter = reader.lines();
    iter.next(); // skip first line (to basically be "on" first line to start)

    while let Some(mut val) = iter.next() {
        for _ in 0..(down-1) {
            if let Some(matched) = iter.next() {
                val = matched;                
            } else {
                return tree_hit;
            }
        }
        let line = val.unwrap();
        current_x += right;
        let offset = current_x % line.len();
        let found = line.chars().nth(offset).unwrap();
        if found == '#' {
            tree_hit += 1;
        }
        if verbose {
            println!("line: {}\noffset: {}\nfound:{}", line, offset, found);
        }
    }
    return tree_hit;
}

fn day3_part1(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    
    let mut current_x = 0;
    let mut tree_hit = 0;

    let mut iter = reader.lines();
    iter.next(); // skip first line

    while let Some(val) = iter.next() {
        let line = val.unwrap();
        current_x += 3;
        let offset = current_x % line.len();
        let found = line.chars().nth(offset).unwrap();
        if found == '#' {
            tree_hit += 1;
        }
        if verbose {
            println!("line: {}\noffset: {}\nfound:{}", line, offset, found);
        }
    }
    return tree_hit;
}

fn day1_part1(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    // treat "values" here as a bitset. set bits to 1 for each value read from the file. 
    let mut values: [u64; 32] = [0; 32];
    for line in reader.lines() {
        if let Ok(val) = line.unwrap().parse::<usize>() {
            // calculate the partner needed for the current value to sum to 2020. 
            // if that partner is already in the bitset, we are done.
            let partner = 2020 - val;
            let partner_byte_offset = partner / 64;
            let partner_bit_offset = partner % 64;
            let mut partner_mask: u64 = 1;
            partner_mask = partner_mask << partner_bit_offset;
            if values[partner_byte_offset] & partner_mask != 0 {
                let answer = val * partner;
                if verbose {
                    println!("found {} and {} for a sum of {}, product {}", val, partner, val + partner, val * partner);
                }
                return answer;
            }
            // otherwise, set the value in the bitset corresponding to the currently read in value
            let byte_offset = val / 64;
            let bit_offset = val % 64;
            let mut mask: u64 = 1;
            mask = mask << bit_offset;
            values[byte_offset] = values[byte_offset] | mask;
        }
    }
    return 0;
}

fn day1_part2(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    // like part 1, use a bitset as an "index", but this time, read in the full list of values, setting 
    // all corresponding bits in the index. 
    let mut index: [u64; 32] = [0; 32];

    // also, keep track of the raw values seen as a vector. this is for convenience. could determine this
    // from the index but reverse masking out and shifting, etc. but decided not to do that and take the
    // memory hit.
    let mut values = Vec::new();
    for line in reader.lines() {
        if let Ok(val) = line.unwrap().parse::<usize>() {
            values.push(val);
            let byte_offset = val / 64;
            let bit_offset = val % 64;
            let mut mask: u64 = 1;
            mask = mask << bit_offset;
            index[byte_offset] = index[byte_offset] | mask;
        }
    }
    // now that index is loaded up, iterate over each value read. Once we are looking at a value, we can 
    // subract that from 2020, and the result is the "partial" sum we are now looking for 2 other numbers
    // to sum to. Which is a lot like what we did for the first part of the problem. 
    // so outer loop to start the process with the first number...
    for val in &values {
        let partial = 2020 - val;
        // inner loop to start the process like part 1 of finding a single partner within the index that would
        // sum to the "partial" we are looking for. 
        for val2 in &values {
            // skip the value we are on in the outer loop. seems safe here to assume no dups. 
            if *val2 == *val {
                continue;
            }
            // if the value we are on is itself larger than the partial we are looking for, then we can skip it
            if *val2 > partial {
                continue;
            }
            // otherwise, see if the partner is set in the index
            let partner = partial - val2;
            let partner_byte_offset = partner / 64;
            let partner_bit_offset = partner % 64;
            let mut partner_mask: u64 = 1;
            partner_mask = partner_mask << partner_bit_offset;
            if index[partner_byte_offset] & partner_mask != 0 {
                let answer = val * val2 * partner;
                if verbose {
                    println!("found {}, {}, and {} for a sum of {}, product {}", val, val2, partner, val + val2 + partner, val * val2 * partner);
                }
                return answer;
            }
        }
    }
    return 0;
}

#[derive(Debug)]
struct Rule {
    min:    usize,
    max:    usize,
    letter: char,
}

fn day2_part1(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    
    let mut good_count = 0;
    for line in reader.lines() {
        let val = line.unwrap();
        let sep_index = val.find(':').unwrap();
        let rule_str = &val[..sep_index];
        let rule = parse_rule(rule_str);
        let mut count = 0;
        let password = &val[sep_index+2..];

        for letter in password.chars() {
            if letter == rule.letter {
                count += 1;
            }
        }
        if count >= rule.min && count <= rule.max {
            good_count += 1;
        } else if verbose {
            println!("bad password. rule: {:?}, rule_str: {}, pass: {}", rule, rule_str, password)
        }    
    }
    return good_count;
}

fn day2_part2(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    
    let mut good_count = 0;
    for line in reader.lines() {
        let val = line.unwrap();
        let sep_index = val.find(':').unwrap();
        let rule_str = &val[..sep_index];
        let rule = parse_rule(rule_str);

        let password = &val[sep_index+2..];
        let mut count = 0;
        if password.len() >= rule.min && password.chars().nth(rule.min-1).unwrap() == rule.letter {
            count += 1;
        }
        if password.len() >= rule.max && password.chars().nth(rule.max-1).unwrap() == rule.letter {
            count += 1;
        }

        if count == 1 {
            good_count += 1;
        } else if verbose {
            println!("bad password. rule: {:?}, rule_str: {}, pass: {}", rule, rule_str, password)
        }    
    }
    return good_count;
}

fn day2_part2_mapped(fname: &str, verbose: bool) -> usize {
    let reader = BufReader::new(File::open(fname).unwrap());
    
    return reader.lines().map(|line| {
        let val = line.unwrap();
        let sep_index = val.find(':').unwrap();
        let rule_str = &val[..sep_index];
        let rule = parse_rule(rule_str);

        let password = &val[sep_index+2..];
        let mut count = 0;
        if password.len() >= rule.min && password.chars().nth(rule.min-1).unwrap() == rule.letter {
            count += 1;
        }
        if password.len() >= rule.max && password.chars().nth(rule.max-1).unwrap() == rule.letter {
            count += 1;
        }

        if count == 1 {
            return 1;
        } else if verbose {
            println!("bad password. rule: {:?}, rule_str: {}, pass: {}", rule, rule_str, password)
        }    
        return 0;
    }).sum::<usize>();
}


fn parse_rule(rule_str: &str) -> Rule {
    let dash_index = rule_str.find('-').unwrap();
    let space_index = rule_str.find(' ').unwrap();

    return Rule {
        min: rule_str[..dash_index].parse::<usize>().unwrap(),
        max: rule_str[dash_index+1..space_index].parse::<usize>().unwrap(),
        letter: rule_str.chars().nth(space_index+1).unwrap(),
    }
}